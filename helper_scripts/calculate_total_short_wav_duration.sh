#!/bin/bash

# This script helps us calculate the number of audio files that can be
# used in the dataset; the training segments need to be 30" or shorter.
# Example usage: bash calculate_duration.sh /path/to/your/wav/files 30

# Directory containing the .wav files
if [ -z "$1" ]; then
  echo "Please provide a directory with .wav files"
  exit 1
fi
directory=$1
# each wav file duration should be max 27" or
# the argument given by the user
max_file_duration=${2:-27}

# Initialize total duration
total_duration=0

echo "Calculating the total duration of usable audio files"
for file in "$directory"/*.wav; do
	# Get the duration of the current file in seconds using soxi
	duration=$(soxi -D "$file")
		
	# Check if the duration is less than or equal to max_file_duration seconds
	if (( $(echo "$duration <= $max_file_duration" | bc -l) )); then
		# Add the duration of the current file to the total duration
		total_duration=$(echo "$total_duration + $duration" | bc)
		fi
	done

echo "Total duration of WAV files <= $max_file_duration seconds: $total_duration seconds"
