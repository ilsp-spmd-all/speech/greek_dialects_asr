#!/bin/bash

if [ $# -lt 2 ]; then
  echo "Error: Please provide both i) the root folder that\
       contains subfolders, and ii) the target directory. E.g.:\
       'bash flatten_folders.sh original_corpus final_flat_corpus'"
  exit 1
fi
# Define the root directory containing subfolders
root_dir="$1"

# Define the target directory where all files will be moved
target_dir="$2"

# Create the target directory if it doesn't exist
mkdir -p "$target_dir"

# Loop through all files in subdirectories of the root directory
find "$root_dir" -mindepth 2 -type f -print0 | while IFS= read -r -d '' file; do
    # Extract the filename from the path
    filename=$(basename "$file")
    
    # Move the file to the target directory
    mv "$file" "$target_dir/$filename"
done

echo "All files have been moved to $target_dir"
