#!/bin/bash

# Directory containing the WAV files
if [ -z "$1" ]; then
  echo "Please provide a directory with .wav files"
  exit 1
fi
DIRECTORY=$1

echo "Checking the duration of each file in $DIRECTORY. \
They should be max 30s each, otherwise you will see an error."
# Loop through each WAV file in the directory
for file in "$DIRECTORY"/*.wav; do
    # Use soxi to get the duration of the file in seconds
    duration=$(soxi -D "$file")
    
    # Check if the duration is greater than 30 seconds
    if (( $(echo "$duration > 30" | bc -l) )); then
        echo "$file is longer than 30s (Duration: $duration s)"
    fi
done

echo "Done processing."
