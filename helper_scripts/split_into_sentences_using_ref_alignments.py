"""
Script to split the original audio files into smaller sentence segments using the alignments AND the reference files
"""
import argparse
import glob
import os
import sys
from itertools import chain

import nltk
import pandas as pd
from nltk.tokenize import sent_tokenize
from pydub import AudioSegment


def parse_arguments():
    """helper script to parse arguments"""
    parser = argparse.ArgumentParser(
        description="Split large audio files into smaller ones using the alignments "
        "and a sentence tokenizer",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--wav",
        type=str,
        help="the audio file that will be segmented",
    )
    parser.add_argument(
        "--ref",
        type=str,
        help="The reference txt file with the transcription",
    )
    parser.add_argument(
        "--audio-dir",
        type=str,
        help="Path to the folder containing the original .wav file(s) and the corresponding "
        ".txt reference file(s).",
    )
    parser.add_argument(
        "--alignments",
        type=str,
        required=True,
        help="Path to a single alignment file or to a folder containing multiple alignment files.",
    )
    parser.add_argument(
        "--prefix-alignments",
        type=str,
        default="alignments_",
        help="The prefix of the alignment file. For instance, for the reference file "
        "'R_41_n5WzHj.txt' the alignment file is 'alignments_R_41_n5WzHj.txt'",
    )
    parser.add_argument(
        "--delimiter-alignment",
        default="\t",
        help="The delimiter token of the alignments .csv file.",
    )
    parser.add_argument(
        "--output-dir",
        default="audio_aligned_segments",
        help="The output dir that will contain all audio segments",
    )
    parser.add_argument(
        "--encoding",
        default="utf-8",
        help="The file encoding",
    )
    return parser.parse_args()


def get_sentences_from_tokenizer(reference_txt, encoding):
    """use the tokenizer to fetch the sentences from a text file"""
    tokenizer_sentences = []
    with open(reference_txt, encoding=encoding) as file_handler:
        file_content = file_handler.read()
        # split new lines and tokenize content
        tokenizer_sentences = [line.split("\n") for line in sent_tokenize(file_content)]

        # flatten list and remove empty lines
        tokenizer_sentences = [s for s in chain.from_iterable(tokenizer_sentences) if s]
    return tokenizer_sentences


def string_contains_alphanum(text):
    """Checks if any character in a string is alphanumeric"""
    for char in text:
        if char.isalnum():
            return True
    return False


if __name__ == "__main__":
    try:
        nltk.data.find("tokenizers/punkt")
    except LookupError:
        # download the tokenizer if it doesn't already exist
        nltk.download("punkt")

    args = parse_arguments()

    if not (
        (args.ref and args.wav) or (args.audio_dir and os.path.isdir(args.audio_dir))
    ):
        sys.exit(
            "Please provide either a .wav file (--wav) and its reference .txt file "
            " (--ref) or a folder (--audio-dir) containing multiple audio and reference files."
        )

    os.makedirs(args.output_dir, exist_ok=True)

    ref = []
    paths = []
    alignments = []
    if args.audio_dir:
        audio_paths = glob.glob(os.path.join(args.audio_dir, "*.wav"))
        for audio_fname in audio_paths:
            reference_fname = f"{os.path.splitext(audio_fname)[0]}.txt"

            alignments_file = os.path.join(
                args.alignments,
                f"{args.prefix_alignments}{os.path.basename(reference_fname)}",
            )
            # proceed only if there's a reference and alignments file
            if os.path.isfile(reference_fname) and os.path.isfile(alignments_file):
                # load and normalize the reference file
                ref.append(reference_fname)
                paths.append(audio_fname)
                alignments.append(alignments_file)

    else:
        # otherwise just append the single example provided
        paths.append(args.wav)
        # append the reference file
        ref.append(args.ref)
        if os.path.isfile(args.alignments):
            alignments.append(args.alignments)
        else:
            # if args.alignments was not a file, it is a folder containing multiple alignment files
            alignments_file = os.path.join(
                args.alignments, f"{args.prefix_alignments}{os.path.basename(args.ref)}"
            )
            if not os.path.isfile(alignments_file):
                sys.exit(
                    f"You need to provide an alignment file, such as {alignments_file}"
                )
            alignments.append(alignments_file)

    failed_segments = []
    for reference_file, audio_file, alignments_file in zip(ref, paths, alignments):
        alignment_df = pd.read_csv(alignments_file, delimiter=args.delimiter_alignment)

        sentences = get_sentences_from_tokenizer(reference_file, args.encoding)
        word_counter = -1
        waveform = AudioSegment.from_wav(audio_file)
        for idx, sentence in enumerate(sentences, start=1):
            if not string_contains_alphanum(sentence):
                # ensure that the sentence contains words, otherwise skip it
                continue
            onset_ms = int(alignment_df["onset"][word_counter + 1] * 1000)
            words = [
                word
                for word in nltk.word_tokenize(sentence)
                if word.replace("'", "").isalnum()
            ]
            word_counter += len(words)

            offset_ms = int(alignment_df["offset"][word_counter] * 1000)
            segment = waveform[onset_ms:offset_ms]
            segment_fname = os.path.join(
                args.output_dir,
                os.path.basename(audio_file).replace(".wav", f"_{idx}.wav"),
            )
            segment.export(segment_fname, format="wav")
            reference_fname = segment_fname.replace(".wav", ".txt")
            with open(reference_fname, "w", encoding="utf-8") as fhandler:
                fhandler.write(sentence)
