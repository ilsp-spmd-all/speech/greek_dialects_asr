import textgrid
import os
import argparse

def convert_textgrid_to_txt(input_path):
    # Check if the input path is a directory
    if os.path.isdir(input_path):
        # Process each file in the directory
        for root, dirs, files in os.walk(input_path):
            for file in files:
                if file.endswith(".TextGrid"):
                    file_path = os.path.join(root, file)
                    process_textgrid(file_path)
    elif os.path.isfile(input_path) and input_path.endswith(".TextGrid"):
        # Process a single file
        process_textgrid(input_path)
    else:
        print("No TextGrid files found or the path is invalid.")

def process_textgrid(file_path):
    try:
        # Load the TextGrid file
        tg = textgrid.TextGrid.fromFile(file_path)

        # Prepare the output file name
        base_name = os.path.splitext(file_path)[0]
        output_file_path = f"{base_name}.txt"

        # Open the output file for writing
        with open(output_file_path, 'w') as output_file:
            # Process each tier
            for tier in tg.tiers:
                # Process each interval in the tier
                for interval in tier:
                    if interval.mark.strip():  # Only write non-empty labels
                        output_file.write(f"{interval.mark}\n")
                output_file.write("\n")  # Add a newline for separation between tiers

        print(f"Converted '{file_path}' to '{output_file_path}'")

    except Exception as e:
        print(f"An error occurred processing {file_path}: {e}")

def main():
    parser = argparse.ArgumentParser(description="Convert TextGrid files to text files.")
    parser.add_argument("path", help="Path to a TextGrid file or directory containing TextGrid files.")
    args = parser.parse_args()

    convert_textgrid_to_txt(args.path)

if __name__ == "__main__":
    main()

