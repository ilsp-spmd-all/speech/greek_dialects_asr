#!/bin/bash

audio_folder=$1

if [[ -z $audio_folder ]]; then
	# echo 'No input folder provided; will use current folder.'
	audio_folder=''
elif [[ ${audio_folder:-1} != "/" ]]; then
	# add a backslash at the end of the directory if it wasn't provided
	audio_folder="${audio_folder}/"
fi

for i in $audio_folder*.wav; do
	dur=$(soxi -D $i);
	echo $i,$dur;
done
