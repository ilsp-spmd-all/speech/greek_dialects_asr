# Helper scripts to pre-process the audio files

## Convert all files in a folder to 16kHz mono

To convert the sample rate of all .wav files in a folder, run the following script with the input folder (e.g., `audio_dir`) as the only argument:

`bash convert_to_16kHz.sh audio_dir/`

It will automatically check if an audio file's sample rate is 16kHz, and if not, it will convert it.

## Convert all .doc/.docx files to .txt

`python convert_docx_to_text.py --corpus-path path/to/transcripts`

# Segment audio/reference files based on .TextGrid and .eaf

`python segment_audio_reference_based_on_textgrid_and_elan.py -h`

```
usage: Script that splits an audio file into smaller segments based on a .TextGrid/.eaf file
       [-h] --corpus-path CORPUS_PATH [--tier-name-starts-with TIER_NAME_STARTS_WITH]
       [--silence-labels SILENCE_LABELS] [--praat-files-only] [--elan-files-only] [--output-path OUTPUT_PATH]

optional arguments:
  -h, --help            show this help message and exit
  --corpus-path CORPUS_PATH
                        A path to the corpus containing .TextGrid or .eaf files (default: None)
  --tier-name-starts-with TIER_NAME_STARTS_WITH
                        If you want to use only a specific tier name, provide the string with which it starts
                        (optional) (default: None)
  --silence-labels SILENCE_LABELS
                        Optional label that marks a silence in a segment (default: None)
  --praat-files-only    Use this flag if you want to process .TextGrid files only (default: False)
  --elan-files-only     Use this flag if you want to process .eaf files only (default: False)
  --output-path OUTPUT_PATH
                        The folder in which the results will be stored (default: .)
```

## Normalize transcription files

`python normalize_transcription_dir.py -h`

```
usage: normalize_transcription_dir.py [-h] --txt-path TXT_PATH [--out-file OUT_FILE] [--encoding ENCODING] [--lang LANG]
                                      [--nj NJ] [--lowercase] [--remove-accents] [--remove-punctuation]
                                      [--lexicalize-dates-and-numbers]

Script to preprocess a text for ASR training and testing

optional arguments:
  -h, --help            show this help message and exit
  --txt-path TXT_PATH, --text TXT_PATH, -t TXT_PATH
                        Path to text folder (default: None)
  --out-file OUT_FILE   Path to output text file (default: None)
  --encoding ENCODING   Text file encoding. (default: utf-8)
  --lang LANG           Language code; it is mainly needed for number/date
                        conversion during normalization, or for the SpaCy
                        model during tokenization. For instance, use 'pk'
                        for Pomak or 'el' for Greek. If left empty, the dates
                        and numbers will not be converted. (default: None)
  --nj NJ               Number of jobs to use. Single thread by default (default: 1)
  --lowercase, --no-lowercase
                        Convert to lowercase (default: True)
  --remove-accents, --no-remove-accents
                        Remove accents from characters (default: False)
  --remove-punctuation, --no-remove-punctuation
                        Remove punctuation from sentences (default: True)
  --lexicalize-dates-and-numbers, --no-lexicalize-dates-and-numbers
                        Convert dates and numbers to lexical representations (default: True)
```

For instance, to normalize the Pomak files in place:

`python helper_scripts/normalize_transcription_dir.py --txt studio_xanthis_16kHz/ref/ --lang pk --nj 40`

If you want to save them in a different folder, use the `--out-file` option.

## Get audio file durations

To get all audio file durations, use the `get_file_durations.sh` script. You can provide a specific folder as a first argument, otherwise the script will look for .wav files in the current folder. Last, the script outputs the durations on the screen; if you want them in a file isntead, redirect the output. E.g.:

`bash get_file_durations.sh /data/scratch/filotis/studio_xanthis_16kHz.sh > studio_duration.csv`

The .csv file will contain one column with the audio path, and one with the audio duration in seconds:

```csv
/data/scratch/filotis/studio_xanthis_16kHz/D_01_speakers_xoVY9q_n5WzHj.wav,273.903188
/data/scratch/filotis/studio_xanthis_16kHz/D_02_speakers_xoVY9q_n5WzHj.wav,301.450188
/data/scratch/filotis/studio_xanthis_16kHz/D_03_speakers_xoVY9q_n5WzHj.wav,263.029875
/data/scratch/filotis/studio_xanthis_16kHz/D_04_speakers_xoVY9q_n5WzHj.wav,262.884750
/data/scratch/filotis/studio_xanthis_16kHz/D_05_speakers_xoVY9q_n5WzHj.wav,412.456062
/data/scratch/filotis/studio_xanthis_16kHz/D_06_speakers_xoVY9q_n5WzHj.wav,324.597562
/data/scratch/filotis/studio_xanthis_16kHz/D_07_speakers_xoVY9q_n5WzHj.wav,318.287562
/data/scratch/filotis/studio_xanthis_16kHz/D_08_speakers_xoVY9q_n5WzHj.wav,217.765437
/data/scratch/filotis/studio_xanthis_16kHz/D_09_speakers_xoVY9q_n5WzHj.wav,367.130688
/data/scratch/filotis/studio_xanthis_16kHz/D_10_speakers_xoVY9q_n5WzHj.wav,195.141938
```

## Calculate total duration of files that are shorter than 30" (or other threshold)

Similarly, we may want to calculate the total duration of short audio segments only, as longer audio files cannot be used for training.

To calculate the total length of usable audio, run:

`bash calculate_duration.sh /path/to/your/wav/files 27`

where `27` in this case is the threshold in seconds (27" max for each audio file)

## Split audio segments into sentences

You can use the `split_into_sentences_using_ref_alignments.py` script to split a large audio file into full sentences using the word alignments and the reference files. To be able to run it, install the requirements:

`pip install -r requirements.txt`

To run it, provide a directory with audio and reference files, and a audio with the alignments. For instance:

```
python split_into_sentences_using_ref_alignments.py --audio-dir /data/scratch/filotis/studio_xanthis_16kHz --alignments /data/scratch/filotis/wav2vec_alignment_files --output-dir pomak_sentence_segments
```

The full argument list is the following:

```
usage: split_into_sentences_using_ref_alignments.py [-h] [--wav WAV] [--ref REF] [--audio-dir AUDIO_DIR]
                                                    --alignments ALIGNMENTS
                                                    [--prefix-alignments PREFIX_ALIGNMENTS]
                                                    [--delimiter-alignment DELIMITER_ALIGNMENT]
                                                    [--output-dir OUTPUT_DIR] [--encoding ENCODING]

Split large audio files into smaller ones using the alignments and a sentence tokenizer

optional arguments:
  -h, --help            show this help message and exit
  --wav WAV             the audio file that will be segmented (default: None)
  --ref REF             The reference txt file with the transcription (default: None)
  --audio-dir AUDIO_DIR
                        Path to the folder containing the original .wav file(s) and the corresponding .txt
                        reference file(s). (default: None)
  --alignments ALIGNMENTS
                        Path to a single alignment file or to a folder containing multiple alignment files.
                        (default: None)
  --prefix-alignments PREFIX_ALIGNMENTS
                        The prefix of the alignment file. For instance, for the reference file 'R_41_n5WzHj.txt'
                        the alignment file is 'alignments_R_41_n5WzHj.txt' (default: alignments_)
  --delimiter-alignment DELIMITER_ALIGNMENT
                        The delimiter token of the alignments .csv file. (default: )
  --output-dir OUTPUT_DIR
                        The output dir that will contain all audio segments (default: audio_aligned_segments)
  --encoding ENCODING   The file encoding (default: utf-8)
```

However, it is recommended that you use the **silent pause segmentor script** that only requires the alignment files:

[https://gitlab.com/ilsp-spmd-all/filotis/silent-pause-segmentation](https://gitlab.com/ilsp-spmd-all/filotis/silent-pause-segmentation)