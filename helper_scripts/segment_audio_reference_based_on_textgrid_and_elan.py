import argparse
import math
import os
import re
from pathlib import Path
from typing import List

import pympi
from praatio import audio, textgrid
from pydub import AudioSegment


def parse_arguments():
    """helper script to parse command line arguments"""
    parser = argparse.ArgumentParser(
        "Script that splits an audio file into smaller segments based on a .TextGrid/.eaf file",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--corpus-path",
        type=str,
        help="A path to the corpus containing .wav files along with .TextGrid or .eaf files "
        "(e.g., sample1.wav, sample1.TextGrid)",
        required=True,
    )
    parser.add_argument(
        "--tier-name-starts-with",
        type=str,
        help="If you want to use only a specific tier name, provide the string with which it starts (optional)",
    )
    parser.add_argument(
        "--file-suffix",
        type=str,
        default='',
        help="If your annotation file ends with a certain suffix (e.g., sample1_gen.TextGrid istead of "
        "sample1.TextGrid), provide the _gen suffix.",
    )
    parser.add_argument(
        "--silence-labels",
        type=str,
        help="Optional label that marks a silence in a segment",
    )
    parser.add_argument(
        "--praat-files-only",
        default=False,
        action="store_true",
        help="Use this flag if you want to process .TextGrid files only",
    )
    parser.add_argument(
        "--elan-files-only",
        default=False,
        action="store_true",
        help="Use this flag if you want to process .eaf files only",
    )
    parser.add_argument(
        "--output-path",
        type=str,
        default=".",
        help="The folder in which the results will be stored",
    )
    return parser.parse_args()


def convert_audio_to_16k_mono(input_path):
    """Function to convert audio to 16k mono if needed"""
    export_new = False
    audio_info = AudioSegment.from_file(input_path)
    if audio_info.channels != 1:
        audio_info = audio_info.set_channels(1)
        export_new = True
    if audio_info.frame_rate != 16000:
        audio_info = audio_info.set_frame_rate(16000)
        export_new = True
    if export_new:
        os.rename(input_path, f"{input_path}.BK")
        # export under the same filename
        audio_info.export(input_path, format="wav")
        os.remove(f"{input_path}.BK")


def append_duplicated_suffix(fname, suffix="_dupl"):
    # Split the filename into name and extension
    name, extension = fname.rsplit(".", 1)
    # Add the suffix to the name
    return f"{name}{suffix}.{extension}"


def split_praat_audio_and_text(
    wav_path: str,
    textgrid_path: str,
    tier_name: str,
    output_path: str,
    silence_labels: List = ["", "xxx", ";", "silences"],
):
    """
    This is a modified version of the original splitAudioOnTier Praat script
    (http://timmahrt.github.io/praatIO/praatio/praatio_scripts.html#splitAudioOnTier)
    The main difference is that the modified function also outputs the reference text
    (label) along with the wav segment for each textgrid entry
    Args:
        wav_path:
        textgrid_path:
        tier_name:
        output_path:
        silence_labels: the labels for silent regions.  If silences are
            unlabeled intervals (i.e. blank) then leave this alone.  If
            silences are labeled using praat's "annotate >> to silences"
            then this value should be "silences"
    """
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    tg = textgrid.openTextgrid(textgrid_path, False, duplicateNamesMode="rename")
    entries = tg.getTier(tier_name).entries

    if silence_labels is not None:
        entries = [entry for entry in entries if entry.label not in silence_labels]
    # Calculate the order of magnitude if entries is not empty
    order_of_magnitude = (
        int(math.floor(math.log10(len(entries)))) if len(entries) else 0
    )

    # Build the output name template. We want one more zero in the output
    # than the order of magnitude
    outputTemplate = "%s_%%0%dd" % (Path(wav_path).stem, order_of_magnitude + 1)

    convert_audio_to_16k_mono(wav_path)

    # Output wave files
    wav_object = audio.QueryWav(wav_path)
    for i, entry in enumerate(entries):
        start, end, label = entry
        if not label:
            # skip empty segments
            continue
        output_name = outputTemplate % i

        output_full_path = os.path.join(output_path, output_name + ".wav")
        reference_text_path = output_full_path.replace(".wav", ".txt")

        if os.path.exists(output_full_path):
            output_full_path = append_duplicated_suffix(fname=output_full_path, suffix="_dupl")
            reference_text_path = append_duplicated_suffix(fname=reference_text_path, suffix="_dupl")
            print(
                f"Files existed before ({output_path}/{output_name}) or intervals exist with the same name. "
                f"Renaming to {output_full_path}, {reference_text_path}"
            )
        try:
            frames = wav_object.getFrames(start, end)
            wav_object.outputFrames(frames, output_full_path)

            with open(reference_text_path, "w", encoding="utf-8") as fhandler:
                fhandler.write(label)
        except Exception as e:
            print(f"Skipping segment {start}, {end}; {e}")


def split_eaf_audio_and_text(
    wav_path, eaf_fname, output_path, tier_name_starts_with, silence_labels
):
    """Function that splits an audio file into segments based on its ELAN reference script"""
    eafob = pympi.Elan.Eaf(eaf_fname)
    if tier_name_starts_with:
        tier_names = [
            tn
            for tn in eafob.get_tier_names()
            if tn.startswith(args.tier_name_starts_with)
        ]
    else:
        tier_names = eafob.get_tier_names()

    for tier in tier_names:
        tier_suffix = f"_{tier}" if len(tier_names) > 2 else ""
        tier_output_path = f"{output_path}{tier_suffix}"
        if not os.path.exists(tier_output_path):
            os.mkdir(tier_output_path)

        # Build the output name template
        name = Path(wav_path).stem
        entries = eafob.get_annotation_data_for_tier(tier)
        # We want one more zero in the output than the order of magnitude
        outputTemplate = "%s_%%0%dd" % (
            name,
            int(math.floor(math.log10(len(entries)))) + 1,
        )

        convert_audio_to_16k_mono(wav_path)
        # Output wave files
        wav_object = audio.QueryWav(wav_path)
        for i, annotation in enumerate(entries):
            start, end, label = annotation
            if silence_labels is not None and label in silence_labels:
                # ignore this segment
                continue

            output_name = outputTemplate % i
            output_full_path = os.path.join(tier_output_path, output_name + ".wav")
            reference_text_path = output_full_path.replace(".wav", ".txt")

            if os.path.exists(output_full_path):
                print(
                    f"Overwriting wave files in: {tier_output_path}\n"
                    "Files existed before or intervals exist with "
                    f"the same name: {output_name}"
                )

            wav_object.outputFrames(
                wav_object.getFrames(start / 1000, end / 1000), output_full_path
            )

            with open(reference_text_path, "w", encoding="utf-8") as fhandler:
                fhandler.write(label)


def replace_spaces_with_underscore(input_str):
    return re.sub("\s", "_", f"_{input_str}")


def get_audio_filename(
    annotation_filename, annotation_file_suffix, annotation_file_ext
):
    # the annotation file might contain an optional underscore
    wav_fname = re.sub(rf"(_?).{annotation_file_ext}", ".wav", str(annotation_filename))
    if not os.path.isfile(wav_fname) or ".wav" not in wav_fname:
        # if not found, try removing the TextGrid suffix (i.e., the latest underscore)
        wav_fname = re.sub(
            rf"(_?){annotation_file_suffix}.{annotation_file_ext}",
            ".wav",
            str(annotation_filename),
        )
        if not os.path.isfile(wav_fname) or ".wav" not in wav_fname:
            print(
                f"Skipping {annotation_filename}: Corresponding audio file ({wav_fname}) not found."
            )
            return None
    return wav_fname


if __name__ == "__main__":
    args = parse_arguments()
    if args.output_path != ".":
        os.makedirs(args.output_path, exist_ok=True)
    process_textgrids = not args.elan_files_only
    process_elan = not args.praat_files_only
    if process_textgrids:
        print(f"*{args.file_suffix}.TextGrid")
        text_grid_paths = list(Path(args.corpus_path).rglob(f"*{args.file_suffix}.TextGrid"))
        print("TextGrids:", text_grid_paths)
        for tg_fname in text_grid_paths:
            wav_fname = get_audio_filename(
                tg_fname, args.file_suffix, annotation_file_ext="TextGrid"
            )
            if not wav_fname:
                continue
            print(f"Splitting {wav_fname} and {tg_fname}")
            tg = textgrid.openTextgrid(tg_fname, False, duplicateNamesMode="rename")
            if args.tier_name_starts_with:
                tier_names = [
                    tn
                    for tn in tg.tierNames
                    if tn.startswith(args.tier_name_starts_with)
                ]
            else:
                tier_names = tg.tierNames
            add_suffix = True if len(tier_names) > 1 else False
            base_fname = Path(wav_fname).stem
            for tier in tier_names:
                file_suffix = replace_spaces_with_underscore(tier) if add_suffix else ""
                split_praat_audio_and_text(
                    wav_path=wav_fname,
                    textgrid_path=tg_fname,
                    tier_name=tier,
                    output_path=os.path.join(
                        args.output_path, f"{base_fname}{file_suffix}"
                    ),
                )

    if process_elan:
        # same for ELAN files
        elan_paths = Path(args.corpus_path).rglob(f"*{args.file_suffix}.eaf")
        for eaf_fname in elan_paths:
            wav_fname = get_audio_filename(
                eaf_fname, args.file_suffix, annotation_file_ext="eaf"
            )
            if not wav_fname:
                continue
            print(f"Splitting: {wav_fname} and {eaf_fname}")

            split_eaf_audio_and_text(
                wav_path=wav_fname,
                eaf_fname=eaf_fname,
                output_path=os.path.join(args.output_path, Path(eaf_fname).stem),
                tier_name_starts_with=args.tier_name_starts_with,
                silence_labels=args.silence_labels,
            )
