"""
Module that extracts text from .docx (i.e., MS XML Word) documents.
Code adapted from http://etienned.github.io/posts/extract-text-from-word-docx-simply/
which was in turn inspired by https://github.com/mikemaccana/python-docx)
"""
import argparse
import os
import zipfile
from pathlib import Path
from xml.etree.cElementTree import XML


def parse_arguments():
    """helper script to parse command line arguments"""
    parser = argparse.ArgumentParser(
        "Helper script that converts .docx files to .txt",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--corpus-path",
        type=str,
        help="A path to the corpus containing docx files",
        required=True,
    )
    return parser.parse_args()


def get_docx_text(
    docx_fname,
    namespace="{http://schemas.openxmlformats.org/wordprocessingml/2006/main}",
):
    """
    This function returns the .docx content in unicode
    @docx_fname: the path of a docx file
    """
    document = zipfile.ZipFile(docx_fname)
    xml_content = document.read("word/document.xml")
    document.close()
    tree = XML(xml_content)

    paragraphs = []
    for paragraph in tree.iter(f"{namespace}p"):
        texts = [node.text for node in paragraph.iter(f"{namespace}t") if node.text]
        if texts != []:
            paragraphs.append("".join(texts))

    return paragraphs


if __name__ == "__main__":
    args = parse_arguments()

    docx_paths = Path(args.corpus_path).rglob("*.doc[x]")  # doc or docx
    for docx_fname in docx_paths:
        suffix = Path(docx_fname).suffix
        print(f"Converting: {docx_fname}")
        txt_fname = str(docx_fname).replace(suffix, ".txt")
        if os.path.isfile(txt_fname):
            print(f"{txt_fname} already exists, skipping conversion")
            # make sure the reference .txt file doesn't exist already
            docx_content = get_docx_text(docx_fname)
            if docx_content:
                with open(txt_fname, "w", encoding="utf-8") as fhandler:
                    for line in docx_content:
                        fhandler.write(f"{line}\n")
