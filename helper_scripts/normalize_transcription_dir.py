import argparse
import os
from pathlib import Path

from asr_text_normalizer.normalizer import cfg, normalize_txt


def parse_args():
    parser = argparse.ArgumentParser(description="Script to normalize text files")
    parser.add_argument(
        "--txt-path", help="Path to the text file or directory containing text files"
    )
    parser.add_argument("--encoding", default="utf-8", help="Text file encoding")
    parser.add_argument(
        "--nj", type=int, default=1, help="Number of jobs for parallel processing"
    )
    parser.add_argument(
        "--out-file",
        help="Output file or directory. If left empty, the file will be normalized in place.",
    )
    parser.add_argument(
        "--text-suffix",
        default=".txt",
        help="Text file suffix to look for, default is '.txt'",
    )
    parser.add_argument(
        "--lang",
        help="Provide a language code if you want to lexicalize dates and numbers.",
    )
    # Boolean flags
    parser.add_argument(
        "--lowercase",
        dest="lowercase",
        action="store_true",
        help="Convert to lowercase (default: True)",
    )
    parser.add_argument(
        "--no-lowercase",
        dest="lowercase",
        action="store_false",
        help="Do not convert to lowercase",
    )
    parser.set_defaults(lowercase=True)

    parser.add_argument(
        "--remove-accents",
        dest="remove_accents",
        action="store_true",
        help="Remove accents from characters (default: False)",
    )
    parser.add_argument(
        "--no-remove-accents",
        dest="remove_accents",
        action="store_false",
        help="Do not remove accents from characters",
    )
    parser.set_defaults(remove_accents=False)

    parser.add_argument(
        "--remove-punctuation",
        dest="remove_punctuation",
        action="store_true",
        help="Remove punctuation from sentences (default: True)",
    )
    parser.add_argument(
        "--no-remove-punctuation",
        dest="remove_punctuation",
        action="store_false",
        help="Do not remove punctuation from sentences",
    )
    parser.set_defaults(remove_punctuation=True)

    parser.add_argument(
        "--lexicalize-dates-and-numbers",
        dest="lexicalize_dates_and_numbers",
        action="store_true",
        help="Convert dates and numbers to lexical representations (default: True)",
    )
    parser.add_argument(
        "--no-lexicalize-dates-and-numbers",
        dest="lexicalize_dates_and_numbers",
        action="store_false",
        help="Do not convert dates and numbers to lexical representations",
    )
    parser.set_defaults(lexicalize_dates_and_numbers=True)
    return parser.parse_args()


args = parse_args()
config = cfg.NormalizeConfig.from_args(args)
print(config)
if os.path.isfile(args.txt_path):
    normalize_txt(
        txt_path=args.txt_path,
        encoding=args.encoding,
        config=config,
        nj=args.nj,
        out_file=args.out_file,
    )
elif os.path.isdir(args.txt_path):
    if args.out_file:
        os.makedirs(args.out_file, exist_ok=True)
    for fname in Path(args.txt_path).rglob(f"*{args.text_suffix}"):
        if not args.out_file:
            # in place normalization
            out_file = fname
        else:
            out_file = os.path.join(args.out_file, os.path.basename(fname))
        print(f"Normalizing {fname} -> {out_file}")
        normalize_txt(
            txt_path=fname,
            encoding=args.encoding,
            config=config,
            nj=args.nj,
            out_file=out_file,
        )
