import argparse
import os
from pathlib import Path
from praatio import textgrid
from typing import List, Tuple


def convert_to_seconds(timestamp):
    # Split the timestamp into hours, minutes, seconds, and milliseconds
    parts = timestamp.split(':')
    seconds = 0.0
    if len(parts) == 3:
        # Format is HH:MM:SS.mmm
        hours, minutes, seconds_milliseconds = parts
        seconds += float(hours) * 3600 + float(minutes) * 60
    elif len(parts) == 2:
        # Format is MM:SS.mmm
        minutes, seconds_milliseconds = parts
        seconds += float(minutes) * 60
    elif len(parts) == 1:
        # Format is SS.mmm
        seconds_milliseconds = parts[0]

    # Handle seconds and milliseconds
    if '.' in seconds_milliseconds:
        sec, msec = seconds_milliseconds.split('.')
        seconds += float(sec) + float(msec) / 1000.0
    else:
        seconds += float(seconds_milliseconds)
    
    return seconds

def parse_input_file(input_file_path: str) -> List[Tuple[float, float, str]]:
    with open(input_file_path, "r", encoding="utf-8") as file:
        lines = file.readlines()

    entries = []
    for line in lines:
        if not line.strip():
            # skip empty lines
            continue

        start, end_annotation = line.split(" --> ")
        try:
            end, annotation = end_annotation.split("] ", 1)
        except Exception as e:
            exit(f"{e}:{line}")

        start = start.replace('[', '')
        
        start = convert_to_seconds(start)
        end = convert_to_seconds(end)
        if end < start:
            print(line)
            exit()

        entries.append((start, end, annotation.strip()))

    return entries


def create_textgrid(entries: List[Tuple[float, float, str]], output_file_path: str):
    # Initialize an empty TextGrid object
    tg = textgrid.Textgrid()
    tg.minTimestamp = 0
    tg.maxTimestamp = entries[-1][1]

    interval_list = [(start, end, text) for start, end, text in entries]
    #tier = textgrid.IntervalTier("Annotations", interval_list, interval_list[0][0],  interval_list[-1][1])
    tier = textgrid.IntervalTier("Annotations", interval_list, 0,  interval_list[-1][1])
    
    tg.addTier(tier)
    tg.save(output_file_path, "long_textgrid", includeBlankSpaces=False)



def process_file(file_path: str, output_dir: str):
    print(f"Processing: {file_path}")
    entries = parse_input_file(file_path)
    base_name = os.path.splitext(os.path.basename(file_path))[0]
    output_file_path = os.path.join(output_dir, f"{base_name}.TextGrid")
    create_textgrid(entries, output_file_path)
    print(f"Processed: {file_path} -> {output_file_path}")


def main():
    parser = argparse.ArgumentParser(
        description="Convert annotated text to Praat TextGrid. Accepts both file and folder input."
    )
    parser.add_argument(
        "--input-path",
        type=str,
        help="Path to the input file or folder containing the Whisper txt files "
        "(that contain annotations and reference)",
    )
    parser.add_argument(
        "--output-folder",
        type=str,
        help="Folder where the TextGrid files will be saved",
    )

    args = parser.parse_args()

    # Ensure output directory exists
    if not os.path.exists(args.output_folder):
        os.makedirs(args.output_folder)

    # Check if input path is a file or folder
    if os.path.isfile(args.input_path):
        process_file(args.input_path, args.output_folder)
    elif os.path.isdir(args.input_path):
        for file_name in Path(args.input_path).glob('*.txt'):
            file_path = os.path.join(args.input_path, file_name)
            if os.path.isfile(file_path):
                process_file(file_path, args.output_folder)
    else:
        print("The input path does not exist or is not valid.")


if __name__ == "__main__":
    main()
