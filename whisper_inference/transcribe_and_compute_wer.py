import argparse
import csv
import string

from asr_text_normalizer.normalizer import normalize_string
from jiwer import cer, wer

import whisper


def transcribe_with_whisper(model, audio_path, language):
    """transcribe a WAV file using Whisper"""
    result = model.transcribe(audio_path, language=language)
    return result["text"]


# Function to compute the WER (Word Error Rate) using JiWER
def compute_wer_and_cer(reference, hypothesis):
    word_error = wer(reference, hypothesis)
    character_error = cer(reference, hypothesis)
    return word_error, character_error


def remove_punctuation_and_lowercase(text):
    # Convert the string to lowercase
    lower_case_text = text.lower()
    # Create a translation table that maps each punctuation mark to None
    translator = str.maketrans("", "", string.punctuation)
    # Use the translate method to remove punctuation
    cleaned_text = lower_case_text.translate(translator)
    return cleaned_text


def normalize(text_str, lang_code=None):
    return normalize_string(
        txt=text_str,
        lang=lang_code,
        do_lowercase=True,
        remove_accents=False,
        lexicalize_dates_and_numbers=True if lang_code else False,
        remove_punctuation=True,
    )


def process_csv_and_compute_total_wer(model, csv_path, output_path, language):
    total_wer = 0
    total_cer = 0
    total_sentences = 0

    with open(csv_path, newline="", encoding="utf-8") as csvfile, open(
        output_path, "w", encoding="utf-8"
    ) as outfile:
        reader = csv.DictReader(csvfile)
        outfile.write("path,sentence,transcription,wer,cer\n")
        for row in reader:
            audio_path = row["path"]
            reference_sentence = row["sentence"]
            normalized_reference_sentence = normalize(
                reference_sentence, lang_code=language
            )
            transcription = transcribe_with_whisper(model, audio_path, language)
            normalized_transcription = normalize(transcription, lang_code=language)
            if normalized_transcription == "υπότιτλοι authorwave":
                normalized_transcription = ""
            wer, cer = compute_wer_and_cer(
                normalized_reference_sentence, normalized_transcription
            )
            outfile.write(
                f"{audio_path},{normalized_reference_sentence},{normalized_transcription},{wer},{cer}\n"
            )

            total_wer += wer
            total_cer += cer
            total_sentences += 1

    average_wer = total_wer / total_sentences if total_sentences > 0 else 0
    average_cer = total_cer / total_sentences if total_sentences > 0 else 0
    print(f"Total WER: {average_wer}; total CER: {average_cer}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Transcribe WAV files using Whisper and compute WER"
    )
    parser.add_argument("--dataset", type=str, help="Path to the test set")
    parser.add_argument(
        "--whisper-model", default="large-v3", help="Whisper model name"
    )
    parser.add_argument(
        "--language", default="Greek", help="Whisper model language identification"
    )
    parser.add_argument(
        "--results", default="results_whisper.csv", help="Name of output file"
    )
    args = parser.parse_args()

    print("Loading Whisper model...")
    whisper_model = whisper.load_model(args.whisper_model)
    print("Processing csv and transcribing audio...")
    process_csv_and_compute_total_wer(
        whisper_model, args.dataset, args.results, args.language
    )
