"""API that serves a pre-trained wav2vec2 ASR model"""

import os
import shutil

import torch
from fastapi import FastAPI, UploadFile
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles

import whisper

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

# create a folder to temporarily store the audio files
os.makedirs("uploads", exist_ok=True)

# If the user hasn't specified a model, load the base Whisper one
MODEL_NAME = os.getenv("MODEL_NAME", "base")

# Check if a GPU is available
torch.cuda.is_available()
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
# Load the Whisper model
model = whisper.load_model(MODEL_NAME, device=DEVICE)


@app.post("/transcribe/")
async def create_upload_file(audio: UploadFile):
    """upload a file and transcribe it"""
    with open(f"uploads/{audio.filename}", "wb") as buffer:
        shutil.copyfileobj(audio.file, buffer)

    result = model.transcribe(f"uploads/{audio.filename}")

    # remove the temporary file
    os.remove(f"uploads/{audio.filename}")

    return {"filename": audio.filename, "text": result["text"]}


@app.get("/", response_class=HTMLResponse)
async def main():
    """the default API entrypoint is an HTML form"""
    content = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Transcribe a .wav file</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/static/css/util.css">
        <link rel="stylesheet" type="text/css" href="/static/css/main.css">
        <link rel="stylesheet" type="text/css" href="/static/css/main.css">
    <!--===============================================================================================-->
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form class="login100-form validate-form" action="/transcribe/" enctype="multipart/form-data" method="post">
                        <span class="login100-form-title p-b-26">
                            Transcribe an audio file
                        </span>
                        <div class="wrap-input100">
                            <input name="audio" type="file">
                        </div>
                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    </html>
    """
    return HTMLResponse(content=content)
