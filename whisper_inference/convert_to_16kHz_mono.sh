#!/bin/bash

# use this and -iname to look for .WAV along with .wav
shopt -s nocaseglob
# Set IFS to newline character to properly handle filenames with spaces
IFS=$'\n'

# provide the audio folder as the first argument
input_folder=$1

for i in $(find "$input_folder" -iname "*.wav"); do
	echo $i
	# Check if the file has a .WAV extension
    if [[ $i == *.WAV ]]; then
        # Get the current filename without the extension
        filename=$(basename "$i" .WAV)
        # Rename the file by replacing .WAV with .wav
        mv "$i" "$input_folder/$filename.wav"
        echo "Renamed $i to $filename.wav"
    fi

    srate="$(soxi -r $i)"
	channels="$(soxi -c $i)"
	# if the sample rate isn't 16kHz, convert
	if [[ "$srate" != "16000" ]] || [[ "$channels" != "1" ]]
	then
		tmp_fname="$i.BK.wav"
		ffmpeg -y -i $i -ac 1 -ar 16000 $tmp_fname
		mv $tmp_fname $i
	fi
done
