# Speech-to-text using Whisper

## 1. How to run the dockerized Speech-to-Text API

First, you need to build a Docker image. For better handling, give it a name, for instance `whisper-api`. To build the image, run:

`docker build -t whisper-api -f Dockerfile_api .`

The default Whisper model is `base`. To use a different one, e.g., `large-v2`:

`docker build -t whisper-api --build-arg MODEL_NAME=large-v2 -f Dockerfile_api .`

To run the container, you need to use the built image:

`docker run -p 18048:18048 whisper-api`

To change the port to, e.g., 12345:

`docker run -p 12345:18048 whisper-api`

### How to transcribe using the API

You can either transcribe one file at a time via the terminal:

`curl -F "audio=@/path/to/file" http://0.0.0.0:18048/transcribe`

or use the web-based interface: `http://0.0.0.0:18048`

## 2. How to transcribe using the non-API version

First, you need to build the Docker image. For better handling, give it a name, for instance `whisper-wav`. To build the image, run:

`docker build -t whisper-wav .`

The default Whisper model is `base`. To use a different one, e.g., `large-v2`:

`docker build -t whisper-wav --build-arg MODEL_NAME=large-v2 .`

You can also specify the language (e.g., `Greek` or `el` for Greek), otherwise it will be automatically detected:

`docker build -t whisper-wav --build-arg MODEL_NAME=large-v2 --build-arg LANGUAGE=el .`

### Transcribe a wav file or a folder containing several audio files

To transcribe a folder (e.g., `wav_files`) you need to mount 1) the absolute audio path to `/whisper/audio`, 2) the output folder that will contain the results to `/whisper/results`. For instance:

`docker run --gpus '"device=0"' --name whisper-transcription -v $PWD/wav_files:/whisper/audio -v $PWD/results:/whisper/results whisper-wav`

To run interactively:

`docker run -it --gpus '"device=0"' --name whisper-transcription -v $PWD/wav_files:/whisper/audio -v $PWD/results:/whisper/results whisper-wav /bin/bash`


# 3. Google colab

We have also added a [Google colab notebook](https://colab.research.google.com/drive/1U8ThW6JEOpe_3J7HqVgr-sjxPaZcpB2R?usp=sharing) that allows you to transcribe a single .wav file or a .zip containing one or several audio files. (Go [here](https://colab.research.google.com/drive/1iostHvGyOZSd122Wl-NZHOXBbwhNvr2T?usp=sharing) for the same colab in Greek)
