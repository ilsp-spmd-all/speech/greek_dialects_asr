import argparse
import os
from pathlib import Path

from pydub import AudioSegment


# Function to parse the timestamp into milliseconds
def parse_timestamp(timestamp):
    parts = timestamp.split(":")
    minutes = int(parts[0])
    seconds = int(parts[1].split(".")[0])
    milliseconds = int(parts[1].split(".")[1])
    return (minutes * 60 + seconds) * 1000 + milliseconds


# Function to split the wav file based on the provided annotations
def split_wav_and_transcriptions(wav_dir, annotations_dir, output_path):
    # Ensure output directory exists
    Path(output_path).mkdir(parents=True, exist_ok=True)

    # List all wav files and annotations
    wav_files = [f for f in os.listdir(wav_dir) if f.endswith(".wav")]
    annotation_files = [f for f in os.listdir(annotations_dir) if f.endswith(".txt")]

    for annotation_file in annotation_files:
        # Assuming a naming convention that matches wav files to annotation files
        wav_file = annotation_file.replace(".txt", ".wav")
        if wav_file in wav_files:
            audio_path = os.path.join(wav_dir, wav_file)
            annotations_path = os.path.join(annotations_dir, annotation_file)

            # Load the audio file
            audio = AudioSegment.from_wav(audio_path)

            # Read the annotations file
            with open(annotations_path, "r", encoding="utf-8") as file:
                annotations = file.readlines()

            # Process each annotation line
            for i, line in enumerate(annotations):
                # Parse the timestamp and text
                try:
                    if line.strip():
                        timestamp, text = line.strip().split("]  ")
                        start_ts, end_ts = timestamp[1:].split(" --> ")
                        start_ms = parse_timestamp(start_ts)
                        end_ms = parse_timestamp(end_ts)

                        # Extract the audio segment
                        segment = audio[start_ms:end_ms]
                        # Save the audio segment
                        filename_stem = Path(audio_path).stem
                        segment_name = os.path.join(
                            output_path, f"{filename_stem}_{i+1}.wav"
                        )
                        segment.export(segment_name, format="wav")

                        # Save the transcription
                        transcription_name = os.path.join(
                            output_path, f"{filename_stem}_{i+1}.txt"
                        )
                        with open(
                            transcription_name, "w", encoding="utf-8"
                        ) as transcription_file:
                            transcription_file.write(text)

                        print(
                            f"Segment {i+1} saved as {segment_name} with transcription {transcription_name}"
                        )
                except Exception as e:
                    print(e, line, annotations_path, audio_path)
                    import sys

                    sys.exit()
        else:
            print(f"Annotation file for {wav_file} not found.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Split a WAV file into segments based on the Whisper timestamps."
    )
    parser.add_argument(
        "--wav-path", type=str, help="Path to the input 16kHz mono .wav files"
    )
    parser.add_argument(
        "--transcriptions-path",
        type=str,
        help="Path to the .txt Whisper output file that contains timestamps",
    )
    parser.add_argument("--output", type=str, help="Path to output folder")
    args = parser.parse_args()

    split_wav_and_transcriptions(args.wav_path, args.transcriptions_path, args.output)
