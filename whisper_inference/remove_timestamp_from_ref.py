import argparse
import os
import re


def remove_timestamps_and_newlines(input_file, output_file):
    with open(input_file, "r", encoding="utf-8") as file:
        lines = file.readlines()

    # Initialize an empty string to hold the cleaned and concatenated text
    cleaned_text = ""
    for line in lines:
        cleaned_line = re.sub(
            r"\[\d{2}:\d{2}\.\d{3} --> \d{2}:\d{2}\.\d{3}\]\s*", "", line
        )
        # Add the cleaned line to the cleaned_text string without adding a new line
        cleaned_text += cleaned_line.strip() + " "  # A space is added to separate words

    # Write the single paragraph to the output file
    with open(output_file, "w", encoding="utf-8") as file:
        file.write(
            cleaned_text.strip()
        )  # Strip is used to remove any leading/trailing spaces


def process_directory(input_directory, output_directory):
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    for filename in os.listdir(input_directory):
        if filename.endswith(".txt"):
            input_file = os.path.join(input_directory, filename)
            output_file = os.path.join(output_directory, filename)
            remove_timestamps_and_newlines(input_file, output_file)
            print(f"Processed {filename} -> {output_file}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Remove timestamps from text files.")
    parser.add_argument(
        "--input-path",
        type=str,
        help="Path to the directory containing the text files.",
    )
    parser.add_argument(
        "--output-path",
        type=str,
        help="Path to the directory where the cleaned files will be saved.",
    )

    args = parser.parse_args()

    process_directory(args.input_path, args.output_path)
