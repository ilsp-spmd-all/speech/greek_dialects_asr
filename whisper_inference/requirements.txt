fastapi==0.86.0
uvicorn==0.19.0
python-multipart==0.0.5
pydub==0.25.1
openai-whisper