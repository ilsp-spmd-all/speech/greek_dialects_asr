"""
Script to evaluate audio files using a wav2vec2 model
"""

import argparse
import csv
import os
import re
import sys
import warnings
from pathlib import Path

if "CUDA_VISIBLE_DEVICES" not in os.environ:
    # it's important that this is set BEFORE pytorch/transformers are imported
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import librosa
import torch
from asr_text_normalizer.normalizer import cfg, normalize_txt
from datasets import load_dataset
from jiwer import cer, wer
from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor


def show_percentage(error_rate, dec_point=2):
    """Helper function that rounds up the WER, CER percentages"""
    return round(error_rate * 100, dec_point)


def compute_metrics(references, transcriptions, results_fname, audio_fnames=None):
    """Computes WER and CER metrics given a reference (ground truth)
    and a transcription (prediction)
    @references: ground truth list
    @transcriptions: predictions list
    @results_fname: csv file in which the resutls will be saved
    @audio_fnames: a list with the corresponding audio file names."""
    wer_result = wer(truth=references, hypothesis=transcriptions)
    cer_result = cer(truth=references, hypothesis=transcriptions)
    print(
        "Total WER:",
        show_percentage(wer_result),
        "Total CER:",
        show_percentage(cer_result),
    )

    with open(results_fname, "w", encoding="UTF-8") as fhandler:
        writer = csv.writer(fhandler, quoting=csv.QUOTE_NONNUMERIC)
        # write header
        writer.writerow(["file_name", "reference", "transcription", "wer", "cer"])

        # WER per prediction
        for idx, (prediction, reference) in enumerate(
            zip(transcriptions, references), start=1
        ):
            tmp_wer = show_percentage(wer(truth=reference, hypothesis=prediction))
            tmp_cer = show_percentage(cer(truth=reference, hypothesis=prediction))
            fname = audio_fnames[idx - 1] if audio_fnames is not None else ""
            writer.writerow([fname, reference, prediction, tmp_wer, tmp_cer])


def sorted_alphanumerically(lst):
    """Sort in an alphanumeric manner. Otherwise checkpoint-9925
    will be considered more recent than checkpoint-11910"""

    def convert(text):
        return int(text) if text.isdigit() else text

    alphanum_key = lambda key: [convert(c) for c in re.split("([0-9]+)", key)]
    return sorted(lst, key=alphanum_key)


def file_to_list(filename, encoding="utf-8"):
    """Helper script to load a file into a list"""
    with open(filename, "r", encoding=encoding) as f_in:
        # strip lines and ignore empty ones
        return list(filter(None, (line.rstrip() for line in f_in)))


class SpeechToTextEvaluation:
    """
    Class that loads an XLS-R model, transcribes all files
    in a directory and evaluates them.
    """

    def __init__(self, model_name, model_checkpoint=None, use_cuda=True):
        """Load model and processor"""
        # load one of the huggingface models
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() and use_cuda else "cpu"
        )
        print(f"Running script on {self.device}")
        local_model_name = model_name
        if os.path.isdir(local_model_name):
            suffix = [
                i for i in os.listdir(local_model_name) if i.startswith("checkpoint")
            ]
            if suffix:
                if model_checkpoint:
                    # get the specific checkpoint that was specified
                    suffix_id = [ch for ch in suffix if model_checkpoint in ch][0]
                else:  # if it wasn't specified, just get the latest
                    suffix_id = sorted_alphanumerically(suffix)[-1]
                # if there are multiple, get the last checkpoint
                local_model_name = os.path.join(local_model_name, suffix_id)
                print("Using checkpoint:", local_model_name)
        self.model = Wav2Vec2ForCTC.from_pretrained(local_model_name).to(self.device)
        # the tokenizer configs, on the other hand, are not inside the checkpoint
        self.processor = Wav2Vec2Processor.from_pretrained(model_name)

    def transcribe(self, speech_array):
        """transcribe an audio array"""
        inputs = self.processor(
            [speech_array],
            sampling_rate=16_000,
            return_tensors="pt",  # padding=True
        ).to(self.device)
        with torch.no_grad():
            logits = self.model(
                inputs.input_values,
                attention_mask=inputs.attention_mask
                if "attention_mask" in inputs
                else None,
            ).logits

        predicted_ids = torch.argmax(logits, dim=-1)
        return self.processor.batch_decode(predicted_ids)

    def evaluate_dir(
        self, audio_dir, results_filename, text_encoding, lang_code, normalize_files
    ):
        """
        Collect all fnames from a dir (assuming that .mp3 and .wav are the only allowed file types)
        Transcribe them and compute the WER/CER metrics
        """
        audio_paths = Path(audio_dir).rglob("*.[wm][ap][v3]")  # wav or mp3

        references = []
        transcriptions = []
        audio_files = []
        if normalize_files:
            normalizer_config = cfg.NormalizeConfig(
                lang=lang_code,
                replace_characters=False,
                do_lowercase=True,
                remove_accents=False,
                lexicalize_dates_and_numbers=True,
                remove_punctuation=True,
            )
        for audio_fname in audio_paths:
            reference_fname = f"{os.path.splitext(audio_fname)[0]}.txt"
            if os.path.isfile(reference_fname):
                audio_files.append(audio_fname)
                speech_array, _ = librosa.load(audio_fname, sr=16000)
                transcriptions += self.transcribe(speech_array)
                if normalize_files:
                    # normalize the reference file. The lang code is important only
                    # if you want to convert numbers into words
                    transcript = normalize_txt(
                        txt_path=reference_fname,
                        encoding=text_encoding,
                        config=normalizer_config,
                        nj=1,
                        out_file=None,
                    )
                else:
                    transcript = file_to_list(reference_fname)
                references.append(" ".join(transcript))
            else:
                print(
                    f"Missing reference file ({reference_fname}). Will exclude audio file "
                    f"({audio_fname.name}) from the evaluation."
                )

        compute_metrics(references, transcriptions, results_filename, audio_files)

    def evaluate_dataset(self, dataset_name, results_filename):
        """
        Load a dataset containing paths to audio files and their normalized reference
        Transcribe them and compute the WER/CER metrics
        """
        # load the test set in the following manner, otherwise it'll be regarded a training split
        data_file = {"test": dataset_name}
        dataset = load_dataset("csv", data_files=data_file)["test"]

        transcriptions = []
        for audio_fname in dataset["path"]:
            speech_array, _ = librosa.load(audio_fname, sr=16000)
            transcriptions += self.transcribe(speech_array)

        compute_metrics(
            references=dataset["sentence"],
            transcriptions=transcriptions,
            results_fname=results_filename,
            audio_fnames=dataset["path"],
        )


def parse_arguments():
    """Parse CLI arguments"""
    parser = argparse.ArgumentParser(
        description="Script to evaluate audio files using a transformer model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--audio-path",
        "-a",
        help="Path to audio files and their transcription. Use this option "
        "if you have a folder you want to evaluate, or the `--dataset-name` flag if you have "
        "generated a dataset (e.g., using create_dataset.py).",
    )
    parser.add_argument(
        "--dataset-name",
        help="The file name of the .csv dataset containing the "
        "normalized reference text and the audio file path",
    )
    parser.add_argument(
        "--model-name",
        "-m",
        help="An XLS-R pre-trained model (local or on hugging face). If you don't know which"
        "one to choose, look for the language family or a related language here: "
        "https://huggingface.co/models?pipeline_tag=automatic-speech-recognition&sort=downloads",
    )
    parser.add_argument(
        "--checkpoint-id",
        help="In case you use a local model and there are several model checkpoints, "
        "you can specify which one to use. If left empty, the latest checkpoint will be used.",
        default=None,
    )
    parser.add_argument(
        "--no-cuda",
        help="Use this flag if you want to evaluate on CPU only",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "--results-fname",
        help="Name of csv file in which the evaluation results will be stored",
        default="results.csv",
    )
    parser.add_argument(
        "--preprocess",
        dest="preprocess",
        action="store_true",
        default=False,
        help="Use this flag if the transcript is *not* normalized and needs to be processed.",
    )
    parser.add_argument(
        "--encoding",
        help="(It is only used when paired with --audio-path. The other option, "
        "--dataset-name, contains normalized texts). Reference text encoding. ",
        default="utf-8",
    )
    parser.add_argument(
        "--lang-code",
        help="(It is only used when paired with --audio-path and --preprocess. "
        "The other option, --dataset-name, contains normalized texts)."
        "The (two-letter) language code (e.g., 'el' for Greek) is only important for "
        "num2words, i.e., for the conversion of numbers into words, which is a language-specific "
        "text normalization step. For more details see "
        "https://gitlab.com/ilsp-spmd-all/filotis/asr-text-preprocessing ",
    )
    args, unknown = parser.parse_known_args()
    return args


if __name__ == "__main__":
    args = parse_arguments()

    asr_eval = SpeechToTextEvaluation(
        model_name=args.model_name,
        model_checkpoint=args.checkpoint_id,
        use_cuda=not args.no_cuda,
    )
    if not (args.audio_path or args.dataset_name):
        sys.exit(
            "You need to either provide a dataset (--dataset-name) or a path to a "
            "directory containing .wav and .txt reference files (--audio-path"
        )
    if args.preprocess and not args.lang_code:
        warnings.warn(
            message="You have not provided a language code; the numbers will not be pre-processed."
        )

    if args.audio_path:
        asr_eval.evaluate_dir(
            audio_dir=args.audio_path,
            results_filename=args.results_fname,
            text_encoding=args.encoding,
            lang_code=args.lang_code,
            normalize_files=args.preprocess,
        )
    elif args.dataset_name:
        asr_eval.evaluate_dataset(
            dataset_name=args.dataset_name, results_filename=args.results_fname
        )
