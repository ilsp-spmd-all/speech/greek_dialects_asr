# How to run the dockerized Speech-to-Text API

First, you need to build a Docker image. For better handling, give it a name, for instance `pomak-api`. To build the image, run:

`docker build -t pomak-api .`

To run the container, you need to use the built image and mount the pretrained wav2vec2 speech-to-text model to the folder `/code/wav2vec2-model` using the `-v` flag. Note that you need to give an absolute path. For instance, if your model is in the current directory under the folder `wav2vec2-pomak-model`, run:

`docker run --rm --name pomak-api-test -v $PWD/wav2vec2-pomak-model:/code/wav2vec2-model -p 18048:18048 pomak-api`

`-p` specifies the port. If you want to use a different port for your service, forward a different port number. For instance, for port 1886, run: `-p 1886:18048`.

To run the docker container in the background, use the `--detach` flag:

`docker run --detach --rm --name pomak-api-test -v $PWD/wav2vec2-pomak-model:/code/wav2vec2-model -p 18048:18048 pomak-api`


## Composer alternative

### .env variant

Create a `/path/to/api/.env` file

```
Path2API=/path/to/api
# give the port number you want to expose
port_number=10101
container_name="test-container-name"
```

Execute:

`docker-compose --env-file  /path/to/api/.env -f /path/to/api/always_up.yml up -d --force-recreate --build`

### one-liner variant

`docker-compose --env-file  $(TMP=$(mktemp) && (echo "Path2API=/path/to/api";echo "port_number=18048";echo "container_name=\"this_bucket\"")>$TMP && echo $TMP ) -f /path/to/api/always_up.yml up -d --force-recreate --build`

or in case already at /path/to/api 

`docker-compose --env-file  $(TMP=$(mktemp) && (echo "Path2API="$PWD;echo "port_number=18048";echo "container_name=\"this_bucket\"")>$TMP && echo $TMP ) -f always_up.yml up -d --force-recreate --build`

or PathInVar=/path/to/api

`docker-compose --env-file  $(TMP=$(mktemp) && (echo "Path2API="$PathInVar;echo "port_number=18048";echo "container_name=\"this_bucket\"")>$TMP && echo $TMP ) -f $PathInVar/always_up.yml up -d --force-recreate --build`


# How to use the API

If you have started the docker, you can either use the UI on the root endpoint, or call directly the `transcribe` endpoint. For instance, if you have set the docker on `chomsky.ilsp.gr:18048`, you can transcribe a `test.wav` file using curl:

`curl -v -F "audio=@test.wav" -F align=False http://chomsky.ilsp.gr:18048/transcribe/`

You can also transcribe other file types, such as .mp3 and .mp4; the file will be automatically converted into a .wav


# ASR inference

To decode a single .wav file, use the following script (from within the `api` folder, otherwise prepend it to the path):

```
usage: speech_to_text.py [-h] [--model-name MODEL_NAME] [--sample-rate SAMPLE_RATE] [--wav WAV]
                         [--include-time-offsets] [--load-whole-file] [--no-cuda]

Generate a text output from a .wav file

optional arguments:
  -h, --help            show this help message and exit
  --model-name MODEL_NAME
                        A hugging face pre-trained model (local or remote).If you don't know which one to choose, look for
                        the language family or a related language [here](https://huggingface.co/models?pipeline_tag=automatic-
                        speech-recognition&sort=downloads). The default is a multilingual model trained on 56 languages.
                        (default: voidful/wav2vec2-xlsr-multilingual-56)
  --sample-rate SAMPLE_RATE, -sr SAMPLE_RATE
                        The wav audio file sample rate (default: 16000)
  --wav WAV             the audio file that will be aligned (default: None)
  --include-time-offsets
                        Use this flag to obtain the time offset of each audio file. (default: False)
  --load-whole-file     Use this flag to load the whole wav file at once instad of chunking it. It
                        provides higher accuracy but it requires lots of memory (>10GB) to function. For
                        large files or Docker containers you will need to increase the memory and
                        possibly use the --no-cuda flag, depending on the file size. (default: False)
  --no-cuda             Use this flag to run on CPUs only, even if a GPU is available (default: False)
```
