"""Speech-to-text basic script"""
import argparse
import os

import librosa
import torch
from transformers import AutoFeatureExtractor, Wav2Vec2ForCTC, Wav2Vec2Processor


def parse_arguments():
    """helper script to parse arguments"""
    parser = argparse.ArgumentParser(
        description="Generate a text output from a .wav file",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--model-name",
        type=str,
        default="voidful/wav2vec2-xlsr-multilingual-56",
        help="A hugging face pre-trained model (local or remote)."
        "If you don't know which one to choose, look for the "
        "language family or a related language here: "
        "https://huggingface.co/models?pipeline_tag=automatic-speech-recognition&sort=downloads "
        "The default is a multilingual model trained on 56 languages.",
    )
    parser.add_argument(
        "--sample-rate", "-sr", default=16000, help="The wav audio file sample rate"
    )
    parser.add_argument(
        "--wav",
        type=str,
        help="the audio file that will be aligned",
    )
    parser.add_argument(
        "--include-time-offsets",
        help="Use this flag to obtain the time offset of each audio file.",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "--load-whole-file",
        help="Use this flag to load the whole wav file at once instad of chunking it."
        " It provides higher accuracy but it requires lots of memory (>10GB) to function. "
        "For large files or Docker containers you will need to increase the memory and possibly "
        "use the --no-cuda flag, depending on the file size.",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "--no-cuda",
        action="store_true",
        default=False,
        help="Use this flag to run on CPUs only, even if a GPU is available",
    )
    return parser.parse_args()


class SpeechToText:
    """
    Class that loads a huggingface model and transcribes a file
    """

    def __init__(self, model_name, use_cuda=False):
        """Load model and processor"""
        # load one of the hugging face models
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() and use_cuda else "cpu"
        )
        local_model_name = model_name
        if os.path.isdir(local_model_name):
            suffix = [
                i for i in os.listdir(local_model_name) if i.startswith("checkpoint")
            ]
            if suffix:
                # if there are multiple, get the last checkpoint
                local_model_name = os.path.join(local_model_name, sorted(suffix)[-1])
                print("Using checkpoint:", local_model_name)
        self.model = Wav2Vec2ForCTC.from_pretrained(local_model_name).to(self.device)
        # the tokenizer configs, on the other hand, are not inside the checkpoint
        self.processor = Wav2Vec2Processor.from_pretrained(model_name)
        self.feature_extractor = AutoFeatureExtractor.from_pretrained(local_model_name)

    def transcribe_chunks(self, wav_file, include_time_offsets, chunking_duration=30):
        """This functions "streams" the wav file
        instead of loading it at once to reduce memory consumption
        @wav_file: the audio file name
        @include_time_offsets: whether to request time offsets
        @chunking_duration: the length of the chunks that are transcribed each time"""
        transcriptions = []

        # Stream over chunks of a few seconds rather than load the full file
        stream = librosa.stream(
            wav_file,
            block_length=chunking_duration,
            frame_length=16000,
            hop_length=16000,
        )

        for speech in stream:
            # loops through the audio chunks
            if len(speech.shape) > 1:
                speech = speech[:, 0] + speech[:, 1]
            transcriptions.append(
                self.transcribe(speech, include_time_offsets=include_time_offsets)
            )

        if include_time_offsets:
            transcript = ""
            word_offsets = []
            # collect the chunks
            for num_segment, segment in enumerate(transcriptions):
                transcript += segment[0]
                # when it comes to time stamps, we need to take into consideration
                # that the current time offsets are for the chunked audio; we can fix this by adding
                # the latest time offset to all time offsets.
                if not word_offsets and segment[1] is not None:
                    # the first pair of time offsets is correct
                    word_offsets.extend(segment[1])
                else:
                    # to correct the time offsets, we need to add the duration of the chunks
                    # it has already seen (in seconds)
                    latest_offset = chunking_duration * num_segment
                    for offsets in segment[1]:
                        word_offsets.append(
                            self.correct_time_offset(offsets, latest_offset)
                        )

            # return transcript and word offsets
            return transcript, word_offsets
        else:
            # return only the transcript
            return " ".join(transcriptions)

    def transcribe(self, speech_array, include_time_offsets):
        """transcribe an audio array
        @speech_array: a speech array, loaded using librosa
        @include_time_offsets: whether to include time offsets for each word token"""
        inputs = self.processor(
            [speech_array], sampling_rate=16_000, return_tensors="pt"  # , padding=True
        ).to(self.device)
        with torch.no_grad():
            try:
                logits = self.model(
                    inputs.input_values,
                    attention_mask=inputs.attention_mask
                    if "attention_mask" in inputs
                    else None,
                ).logits
            except RuntimeError as error:
                torch.cuda.empty_cache()
                # we get CUDA mem error for these large audio files, skip them for now
                return (
                    None,
                    f"Error: {error}. Re-run on the CPU using the `--no-cuda` argument.",
                )

        predicted_ids = torch.argmax(logits, dim=-1)
        # You can even obtain the character offsets for phone alignment
        # if you repeat the time conversion process for char_offsets
        outputs = self.processor.batch_decode(
            predicted_ids,
            output_word_offsets=include_time_offsets,
            output_char_offsets=False,
        )
        if include_time_offsets:
            transcription = outputs.text[0]
            # retrieve word stamps (analogous commands for `output_char_offsets`)
            # outputs = tokenizer.decode(pred_ids, output_word_offsets=True)
            # compute `time_offset` in seconds as product of downsampling ratio and sampling_rate
            time_offset = (
                self.model.config.inputs_to_logits_ratio
                / self.feature_extractor.sampling_rate
            )
            word_offsets = [
                {
                    "word": d["word"],
                    "start_time": round(d["start_offset"] * time_offset, 2),
                    "end_time": round(d["end_offset"] * time_offset, 2),
                }
                for d in outputs.word_offsets[0]
            ]
            return transcription, word_offsets
        transcription = outputs[0]
        return transcription

    @staticmethod
    def correct_time_offset(dict_offset, latest_time_offset):
        """helper function to correct the time offsets"""
        for offset_key in ["start_time", "end_time"]:
            dict_offset[offset_key] = round(
                dict_offset[offset_key] + latest_time_offset, 2
            )

        return dict_offset


if __name__ == "__main__":
    args = parse_arguments()

    asr_service = SpeechToText(model_name=args.model_name, use_cuda=not args.no_cuda)

    if args.load_whole_file:
        # This option loads the whole file into memory at once. It produces slightly
        # more accurate transcriptions, but it's memory consuming; use the chunking
        # option unless you want to transcribe really small files
        speech_array, _ = librosa.load(args.wav, sr=args.sample_rate)

        transcriptions = asr_service.transcribe(
            speech_array, include_time_offsets=args.include_time_offsets
        )
    else:
        transcriptions = asr_service.transcribe_chunks(
            args.wav, include_time_offsets=args.include_time_offsets
        )

    print(transcriptions)
