"""API that serves a pre-trained wav2vec2 ASR model"""
import io
import os
import random
from typing import Union

import librosa
from fastapi import FastAPI, Form, UploadFile
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from pydub import AudioSegment
from speech_to_text import SpeechToText

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

MODEL_NAME = "wav2vec2-model"

asr_service = SpeechToText(model_name=MODEL_NAME, use_cuda=True)


@app.post("/transcribe/")
async def create_upload_file(
    audio: UploadFile,
    align: Union[bool, None] = Form(default=False),
):
    """upload a file and transcribe it"""
    audio_contents = audio.file.read()
    audio_file = io.BytesIO(audio_contents)

    if "wav" not in audio.filename:
        print(f"Converting {audio.filename} to wav")
        # create a random temporary name to export the file as wav
        tmp_file = f"{random.randrange(50000)}{audio.filename}.wav"
        conv = AudioSegment.from_file(audio_file)
        conv.export(tmp_file, format="wav")
        audio_file = tmp_file

    # load file as 16kHz mono
    speech_array, _ = librosa.load(audio_file, sr=16000, mono=True)
    asr_results = asr_service.transcribe(
        speech_array=speech_array, include_time_offsets=align
    )

    if "wav" not in audio.filename and os.path.isfile(audio_file):
        # remove the temporary converted file
        os.remove(audio_file)

    # if there is a GPU-related error, return it here
    if asr_results[0] is None:
        return {"text": None, "alignments": {}, "error": asr_results[1]}

    if align:
        return {"text": asr_results[0], "alignments": asr_results[1], "error": None}
    return {"text": asr_results, "alignments": {}, "error": None}


@app.get("/", response_class=HTMLResponse)
async def main():
    """the default API entrypoint is an HTML form"""
    content = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Transcribe a .wav file</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="/static/css/util.css">
        <link rel="stylesheet" type="text/css" href="/static/css/main.css">
        <link rel="stylesheet" type="text/css" href="/static/css/main.css">
    <!--===============================================================================================-->
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form class="login100-form validate-form" action="/transcribe/" enctype="multipart/form-data" method="post">
                        <span class="login100-form-title p-b-26">
                            Transcribe an audio file
                        </span>
                        <div class="wrap-input100">
                            <input name="audio" type="file">
                        </div>
                        <div class="wrap-input100">
                            <input type="checkbox" id="align" name="align" checked>
                            <label for="align">Include word alignments</label>
                        </div>

                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    </html>
    """
    return HTMLResponse(content=content)
