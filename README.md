# Speech Recognition for Greek Dialects: A Challenging Benchmark

This repo provides speech-to-text solutions for low-resourced languages (e.g., Greek dialects) using XLS-R and Whisper models. 

The following scripts were used to train and evaluate ASR models for 4 Greek dialects: Aivaliot, Cretan, Messenian, Griko. See: 

S. Vakirtzian, C. Tsoukala, S. Bompolas, K. Mouzou, V. Stamou, G. Paraskevopoulos, A. Dimakis, S. Markantonatou, A. Ralli, A. Anastasopoulos, Speech Recognition for Greek Dialects: A Challenging Benchmark, Proceedings of the Annual Conference of the International Speech Communication Association (INTERSPEECH), 2024.


# Steps to fine-tune and evaluate an XLS-R or Whisper model

To be able to fine-tune your own model, make sure you [install the requirements](#how-to-install) and run the following steps:

### Data preparation

1. [Re-sample your audio files](#audio-sample-rate) to .wav 16kHz 16-bit mono (if needed)
2. [Normalize your transcription files](#normalize-transcription-files) (if needed)
   Furthermore, if your transcriptions are in .docx format, use [this helper script](helper_scripts/convert_docx_to_text.py) to convert them to .txt
3. If you have transcribed your files using Praat or ELAN (.TextGrid and .eaf respectively), use [this script](helper_scripts/segment_audio_reference_based_on_textgrid_and_elan.py) to segment the audio and reference files into smaller parts. In this case, skip the audio segmentation part and go directly to [dataset creation and fine-tuning](#dataset-creation-and-fine-tuning)

If your audio files are longer than 30" each and you haven't transcribed them with timestamps (using ELAN or Praat), you need to obtain the speech-text alignments (i.e., onset and offset times for each word) to be able to segment the audio files into shorter ones. For a possible solution, check [this repo](https://gitlab.com/ilsp-spmd-all/filotis/speech_to_text#optional-audio-segmentation).

### Dataset creation and fine-tuning

To fine-tune the model, you can either use the [Dockerfile (see the documentation here)](fine_tune/README.md) or run the following two scripts separately:

6. Use the [dataset creation script](#create-a-dataset) to create the training/evaluation/test splits
7. [Fine-tune](#fine-tune-a-model) a pre-trained model based on a related language (e.g., based on the language family)

## How to install

It is best if you install the packages within a virtual environment. For instance, if you're using conda:

```
# create a virtual environment named, e.g., stt that uses Python version 3.11
conda create -y --name stt python=3.11

# activate the environment
conda activate stt
```

Some packages fail to install via conda. You can install part of them or all of them using pip:

`pip install -r requirements.txt`

If you want to deactivate the environment run: `conda deactivate`

*Note*: Depending on your CUDA version, you might need to install a different PyTorch version, otherwise you might end up with a version that only utilizes the CPU. For instance, for CUDA 11.4 install via:

`pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu114`

To ensure you have properly installed torch:

```
>>> import torch
>>> torch.cuda.is_available()
True
```

You should now be able to run all the following scripts.

## Audio sample rate

Before you start fine-tuning, make sure that your audio files are .wav *16kHz mono 16bit*. If you need to change the sample rate or the number of channels, use [this helper script](helper_scripts/README.md#convert-all-files-in-a-folder-to-16khz-mono)

## Create a dataset

Given a path that contains the audio files and the corresponding reference files in .txt format, you can easily create a .csv dataset using the following script:

`python fine_tune/create_dataset.py -h`

```
usage: Script to create a dataset in a csv format in order to use it with the dataloader when fine-tuning a model
      [-h] --corpus-dir CORPUS_DIR --dataset-name DATASET_NAME
      [--cutoff-duration CUTOFF_DURATION] [--no-split-dataset]
      [--training-decimal-fraction TRAINING_DECIMAL_FRACTION]
      [--eval-decimal-fraction EVAL_DECIMAL_FRACTION]
      [--test-decimal-fraction TEST_DECIMAL_FRACTION] [--seed SEED]
      [--encoding ENCODING] [--lang-code LANG_CODE] [--preprocess]
      [--no-lowercasing] [--exclude-files [EXCLUDE_FILES ...]]

options:
  -h, --help            show this help message and exit
  --corpus-dir CORPUS_DIR
                        A path to the corpus containing the audio files (*.wav) and the respective reference files (*.txt). E.g., /data/scratch/filotis/studio/ on asimov (default: None)
  --dataset-name DATASET_NAME
                        The csv name of the output dataset. Unless you have used --no-split-dataset, the dataset will be split and the prefixes 'test_', 'eval_' and 'training_' will be
                        added to the name. (default: None)
  --cutoff-duration CUTOFF_DURATION
                        Only include audio segments that have a total duration less than the threshold (in seconds). If you do not want that, give a really large threshold. (default:
                        25)
  --no-split-dataset    By default the dataset is split into training, eval, and test datasets. If you do NOT want to split the dataset, use the --no-split-dataset flag. Otherwise, the
                        dataset will be split and the corresponding prefix will be added to each split (e.g., test_dataset.csv). The default split fractions are 0.8-0.1-0.1 for
                        training, eval and test. Use the corresponding flags, e.g., --test-decimal-fraction, to change these. (default: True)
  --training-decimal-fraction TRAINING_DECIMAL_FRACTION
                        Decimal fraction of the training set. It is ignored if you use --no-split-dataset. (default: 0.8)
  --eval-decimal-fraction EVAL_DECIMAL_FRACTION
                        Decimal fraction of the evaluation set. It is ignored if you use --no-split-dataset. (default: 0.1)
  --test-decimal-fraction TEST_DECIMAL_FRACTION
                        Decimal fraction of the test set. It is ignored if you use --no-split-dataset. (default: 0.1)
  --seed SEED           This is used for reproducability together with the training fraction when splitting into training and test sets (default: 42)
  --encoding ENCODING   Text file encoding. (default: utf-8)
  --lang-code LANG_CODE
                        Language code; it is mainly needed for number/date conversion. (default: pk)
  --preprocess          Use this flag if the transcript is *not* normalized and needs to be processed. (default: False)
  --no-lowercasing      Use this flag to disable text lowercasing when pre-processing. The destination value is 'lowercase' which, by default, is set to True. (default: True)
  --exclude-files [EXCLUDE_FILES ...]
                        To exclude certain speakers or file ids from the dataset, give the corresponding speaker/file ids. For instance, --exclude-speakers 9G75fk will exclude all files
                        from speaker 9G75fk. --exclude-speakers 9G75fk R_01 will additionally exclude the audio file R_01 by any speaker (default: None)
```

For instance:

```python fine_tune/create_dataset.py --corpus-dir /home/speech_to_text/cretan_corrected_segments/ --dataset-name cretan_corrected_segments.csv --lang-code el```

will create a training set (`training_cretan_corrected_segments.csv`) with 80% of the total audio segments, a test set (`test_cretan_corrected_segments.csv`) with 10% of the segments, and an evaluation set (`eval_cretan_corrected_segments.csv`) with the final 10% of the segments.
To change the exact percentages use the `--training-decimal-fraction`, `--test-decimal-fraction`, and `--eval-decimal-fraction` arguments. They need to add up to 1.0 otherwise you will receive an error message stating the issue.

If you want one whole dataset instead of training/test/evaluation splits, use the `--no-split-dataset` option:

```python fine_tune/create_dataset.py --corpus-dir /home/speech_to_text/cretan_corrected_segments/ --dataset-name cretan_corrected_segments.csv --no-split-dataset```

This will output a single .csv file (cretan_corrected_segments.csv) containing the pre-processed reference text and the physical path to the wav file.

All output dataset files are in the following format, where `path` is the absolute path and `sentence` the normalized reference text:

```csv
path,sentence
/home/speech_to_text/cretan_corrected_segments/04_kaseta_No_15/04_kaseta_No_15_071.wav,κι η μελεμενιά που ήθελε ψιλή κουβέντα
/home/speech_to_text/cretan_corrected_segments/10_kaseta_No_40_b/10_kaseta_No_40_b_564.wav,κι έχει και κομπολόι με κάτι χάντρες
/home/speech_to_text/cretan_corrected_segments/10_kaseta_No_40_b/10_kaseta_No_40_b_143.wav,από σπίτι σε σπίτι κάνουνε το γυρολόγο
/home/speech_to_text/cretan_corrected_segments/04_kaseta_No_15/04_kaseta_No_15_065.wav,άφεριμ
/home/speech_to_text/cretan_corrected_segments/07_kaseta_No_19_b/07_kaseta_No_19_b_356.wav,μέχρι αύριο
```

Optional steps:
1. If the reference text is not preprocessed, make sure you use the `--preprocess` flag, along with the language code (`--lang-code`) and, optionally, the file encoding if it's not utf-8 (`--encoding`).
2. Most likely the GPU memory will not be able to handle large audio segments when fine-tuning. There is a default duration cut-off threshold that checks the duration of the audio segments and only includes those that are beneath that threshold (in seconds). The default threshold is 25". To alter that threshold, use the `--cutoff-duration` flag. For instance, `--cutoff-duration 30` will exclude from the dataset audio segments that are longer than 30". If you don't want a threshold at all, provide a large number.

# Exclude certain speakers or audio files from the dataset

If you want to exclude a certain speaker or audio file pattern, provide a speaker id or a list of string after the `--exclude-files` flag.

For instance, `--exclude-files n5WzHj R_32` will exclude all files by speaker `n5WzHj` AND any files with the id `R_32`, even if they were read by another speaker.

Currently, all punctuation marks are being removed during processing.

## Fine-tune an XLS-R model

To adapt a wav2vec2 XLS-R model on your own data, use the above generated csv dataset file and the fine_tune/fine_tune_xlsr.py script:

`python fine_tune/fine_tune_xlsr.py -h`

```
usage: Script to fine-tune a wav2vec model [-h] [--pretrained-model PRETRAINED_MODEL] [--lang-family LANG_FAMILY]
                                            --output-model OUTPUT_MODEL [--word-delimiter-token WORD_DELIMITER_TOKEN] --training-dataset-name TRAINING_DATASET_NAME --eval-dataset-name EVAL_DATASET_NAME [--no-cuda] [--epochs EPOCHS]

optional arguments:
  -h, --help            show this help message and exit
  --pretrained-model PRETRAINED_MODEL
                        The hugging face wav2vec2 model that will be fine-tuned. If you don't know which one to choose, look for the language family or a related language here:
                        https://huggingface.co/models?pipeline_tag=automatic-speech-recognition&sort=downloads (default: None)
  --lang-family LANG_FAMILY
                        In case you don't know which pre-trained model to use and you cannot look it up on huggingface, provide the language family and a relevant model will be selected
                        for you.Currently supported options: ['slavic', 'latin', 'baltic', 'arabic', 'cyrillic'] (default: None)
  --output-model OUTPUT_MODEL
                        The name of the resulting fine-tuned model (default: None)
  --word-delimiter-token WORD_DELIMITER_TOKEN
                        The token separating words in the model. The default is a pipe instead of a space to make it more visible (default: |)
  --training-dataset-name TRAINING_DATASET_NAME
                        The file name of the .csv training dataset containing the normalized reference text and the audio file path. See: create_dataset.py (default: None)
  --eval-dataset-name EVAL_DATASET_NAME, --validation-dataset-name EVAL_DATASET_NAME
                        The file name of the .csv validation dataset containing the normalized reference text and the audio file path. See: create_dataset.py (default: None)
  --no-cuda             Whether to use the CPU only (default: False)
  --epochs EPOCHS       Number of training epochs (default: 35)
```

For instance:

`python fine_tune/fine_tune_xlsr.py --training-dataset-name training_cretan_corrected_segments.csv --eval-dataset-name eval_cretan_corrected_segments.csv --output-model wav2vec2-xlsr-cretan`

will fine-tune the default multilingual model `voidful/wav2vec2-xlsr-multilingual-56`.

If you want to fine-tune a different model, use the `--pretrained-model` flag instead.

The default word delimiter token is the pipe. If you want to change it to a blank space, use this flag: `---word-delimiter-token ' '`

*Note*: If you do not set a specific CUDA number, it will look for the first one (CUDA 0). To specify a different one, make sure you assign a number using `CUDA_VISIBLE_DEVICES`:

`CUDA_VISIBLE_DEVICES=3 python fine_tune/fine_tune_xlsr.py --training-dataset-name training_cretan_corrected_segments.csv --eval-dataset-name eval_cretan_corrected_segments.csv --output-model xls-r-cretan_corrected_segments`

### Whisper fine-tuning

Similarly, use the following script to fine-tune a (most likely whisper-medium) model:

`python fine_tune/fine_tune_whisper -h`

## Evaluation and inference

### XLS-R model 

You can use the following script to transcribe and evaluate a wav2vec model on a path containing audio files and their transcription.

`python xlsr_inference/evaluate_xlsr_model.py -h`

```
usage: evaluate_xlsr_model.py [-h] [--audio-path AUDIO_PATH]
                       [--dataset-name DATASET_NAME] [--model-name MODEL_NAME]
                       [--checkpoint-id CHECKPOINT_ID] [--no-cuda]
                       [--results-fname RESULTS_FNAME] [--preprocess]
                       [--encoding TXT_ENCODING] [--lang-code LANG_CODE]

Script to evaluate audio files using a transformer model

optional arguments:
  -h, --help            show this help message and exit
  --audio-path AUDIO_PATH, -a AUDIO_PATH
                        Path to audio files and their transcription. Use this
                        option if you have a folder you want to evaluate, or
                        the `--dataset-name` flag if you have generated a
                        dataset (e.g., using create_dataset.py). (default:
                        None)
  --dataset-name DATASET_NAME
                        The file name of the .csv dataset containing the
                        normalized reference text and the audio file path
                        (default: None)
  --model-name MODEL_NAME, -m MODEL_NAME
                        A hugging face pre-trained model (local or remote).
                        If you don't know whichone to choose, look for the
                        language family or a related language here:
                        https://huggingface.co/models?pipeline_tag=automatic-
                        speech-recognition&sort=downloads (default: None)
  --checkpoint-id CHECKPOINT_ID
                        In case you use a local model and there are several
                        model checkpoints, you can specify which one to use.
                        If left empty, the latest checkpoint will be used.
                        (default: None)
  --no-cuda             Use this flag if you want to evaluate on CPU only
                        (default: False)
  --results-fname RESULTS_FNAME
                        Name of csv file in which the evaluation results will
                        be stored (default: results.csv)
  --preprocess          Use this flag if the transcript is *not* normalized
                        and needs to be processed. (default: False)
  --encoding ENCODING   (It is only used when paired with --audio-path. The
                        other option, --dataset-name, contains normalized
                        texts). Reference text encoding. (default: utf-8)
  --lang-code LANG_CODE
                        (It is only used when paired with --audio-path and
                        --preprocess. The other option, --dataset-name,
                        contains normalized texts).The (two-letter) language
                        code (e.g., 'pk' for Pomak) is only important for
                        num2words, i.e., for the conversion of numbers into
                        words, which is a language-specific text
                        normalization step. For more details see
                        https://gitlab.com/ilsp-spmd-all/filotis/asr-text-
                        preprocessing (default: None)
```

You can either evaluate a folder containing .wav and .txt files, or provide a dataset .csv file. For instance: 

`
python xlsr_inference/evaluate_xlsr_model.py --dataset-name test_cretan_corrected_segments.csv --results-fname results_test_cretan_corrected_segments.csv --model-name xls-r-cretan_corrected_segments
`

will generate a .csv file (`results_test_cretan_corrected_segments.csv`) in the following format:

```csv
"file_name","reference","transcription","wer","cer"
"/home/speech_to_text/cretan_corrected_segments/03_kaseta_No_10_a/03_kaseta_No_10_a_006.wav","το ζυγωτό που το στρώναμε στ ατζιρίτι","το ζυγωτό που το στρώναμε στ ατζηρίτι",14.29,2.7
"/home/speech_to_text/cretan_corrected_segments/03_kaseta_No_10_a/03_kaseta_No_10_a_217.wav","να ξεκουραδώσω κουράδια","να ξεκουραδώσω κουράδια",0.0,0.0
"/home/speech_to_text/cretan_corrected_segments/03_kaseta_No_10_a/03_kaseta_No_10_a_095.wav","τ απαντά αυτός και γυρίζει","τ απαντά αυτός και γυρίζει",0.0,0.0
"/home/speech_to_text/cretan_corrected_segments/03_kaseta_No_10_a/03_kaseta_No_10_a_017.wav","αυτά μπρε βαρβατίζουνε","αυτά μπρε βαρβατίζουνε",0.0,0.0
```

If you want to transcribe new files using your fine-tuned XLS-R model, refer to [this README](xlsr_inference/README.md) in the `xlsr_inference` folder.

### Whisper

To transcribe .wav audio files using a [pre-trained Whisper model](https://github.com/openai/whisper?tab=readme-ov-file#available-models-and-languages), or [transcribe files and compute the WER](whisper_inference/transcribe_and_compute_wer.py) using a fine-tuned Whisper model, go to the [Whisper folder](whisper_inference/README.md).
