# Dockerized ASR (wav2vec2-XLS-R) training

## Build the Docker image

First, you need to build a Docker image. For better handling, give it a name, for instance `xlsr-training`. To build the image, run:

`docker build -t xlsr-training --build-arg pretrained_model=voidful/wav2vec2-xlsr-multilingual-56 .`

If you want to give more arguments instead of using the default ones, provide more `--build-arg`:

`docker build -t xlsr-training --build-arg pretrained_model=voidful/wav2vec2-xlsr-multilingual-56 --build-arg epochs=10 .`

### Available arguments

The options are the same as the ones used for each script. 

For dataset creation (see the script documentation [here](../README.md#create-a-dataset))

```
lang_code (default: None)
preprocess (default: False)
encoding (default: utf-8)
no_lowercasing (default: True)
exclude_files (default: None)
```

Whereas for fine-tuning (see the script documentation [here](../README.md#fine-tune-an-xls-r-model)):

```
pretrained_model (default: None)
lang_family (default: None)
epochs(default: 35)
output_model (default: wav2vec2-output-model)
learning_rate (default: 3e-4)
keep_lm (default: False)
```

## Run the container

To run the container, you need to use the built image and:

- mount the output model folder and the audio and reference folders using the `-v` flag. Note that you need to give an absolute path. For instance, if your files are in the current directory under the folder `corpus`, run:

`docker run --rm --detach --gpus '"device=0"' --name custom-training -v $PWD/corpus:/fine_tune/corpus -v $PWD/output_model:/fine_tune/output_model xlsr-training`

**1. Make sure to provide short audio files (< 30" each) together with their reference .txt files.**
**2. To change the visible CUDA device, edit the --gpus flag, e.g.: --gpus '"device=1"'.**
**3. Change the container name using the --name flag.**

To run the docker container in the foreground, remove the `--detach` flag.

If you get errors and you want to use an existing container interactively, use the container name (e.g., `custom-training`):

`docker exec -it custom-training /bin/bash`

Or you can start it in an interactive manner:

`docker run -it --gpus '"device=0"' --name custom-training -v $PWD/corpus:/fine_tune/corpus -v $PWD/output_model:/fine_tune/output_model xlsr-training /bin/bash`

To see the logs you only need to provide the container name:

`docker logs custom-training`
