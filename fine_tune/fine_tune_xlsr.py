"""
Fine-tune a wav2vec model on pomak data.
This script is based on the following two tutorials:
- https://colab.research.google.com/github/patrickvonplaten/notebooks/blob/master/Fine_Tune_XLSR_Wav2Vec2_on_Turkish_ASR_with_%F0%9F%A4%97_Transformers.ipynb
- https://huggingface.co/comodoro/wav2vec2-xls-r-300m-west-slavic-cv8/tree/main
"""

import argparse
import itertools
import json
import os
import sys
from dataclasses import dataclass
from typing import Dict, List, Optional, Union

from git import Repo

if "CUDA_VISIBLE_DEVICES" not in os.environ:
    # it's important that this is set BEFORE pytorch/transformers are imported
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import librosa
import numpy as np
import torch
from datasets import load_dataset
from jiwer import cer, wer
from transformers import (
    Trainer,
    TrainingArguments,
    Wav2Vec2CTCTokenizer,
    Wav2Vec2FeatureExtractor,
    Wav2Vec2ForCTC,
    Wav2Vec2Processor,
    IntervalStrategy,
    EarlyStoppingCallback,
)

# In practice, there aren't many relevant models for language groups.
# Definitely not for indic groups, for instance. This dict is a very
# rough approximation and perhaps in most cases the multilingual model
# (voidful/wav2vec2-xlsr-multilingual-56) would be more relevant.
DEFAULT_MODEL_PER_LANG_FAMILY = {
    "slavic": "comodoro/wav2vec2-xls-r-300m-west-slavic-cv8",
    "latin": "facebook/wav2vec2-large-romance-voxpopuli-v2",  # or the multilingual?
    "baltic": "facebook/wav2vec2-large-baltic-voxpopuli-v2",
    "arabic": "jonatasgrosman/wav2vec2-large-xlsr-53-arabic",  # though this excludes persian etc
    "cyrillic": "jonatasgrosman/wav2vec2-large-xlsr-53-russian",
}


class Wav2VecFineTuner:
    """class with helper scripts to allow fine-tuning of a wav2vec2 model"""

    def __init__(
        self,
        vocab_json_file,
        word_delimiter_token,
        pretrained_model,
        output_model_name,
        no_cuda,
        epochs,
        learning_rate,
        batch_size,
        mask_time_prob,
        max_steps,
        eval_steps,
    ):
        tokenizer = Wav2Vec2CTCTokenizer(
            vocab_json_file,
            unk_token="[UNK]",
            pad_token="[PAD]",
            word_delimiter_token=word_delimiter_token,
        )

        # feature_size: Speech models take a sequence of feature vectors as an input. While the
        #               length of this sequence obviously varies, the feature size should not. Here,
        #               feature size is 1 bc the model was trained on the raw speech signal
        # padding_value: For batched inference, shorter inputs are padded with a specific value
        # do_normalize: Whether the input should be *zero-mean-unit-variance* normalized or not.
        #               Usually, speech models perform better when normalizing the input
        # return_attention_mask: Whether the model should make use of an attention_mask for batched
        #                        inference. XLSR-Wav2Vec2 models should use the attention_mask
        feature_extractor = Wav2Vec2FeatureExtractor(
            feature_size=1,
            sampling_rate=16000,
            padding_value=0.0,
            do_normalize=True,
            return_attention_mask=True,
        )

        self.processor = Wav2Vec2Processor(
            feature_extractor=feature_extractor, tokenizer=tokenizer
        )
        self.processor.save_pretrained(os.path.basename(output_model_name))

        # do not set the specific cuda device here; use CUDA_VISIBLE_DEVICES instead
        self.device = torch.device(
            "cpu" if (args.no_cuda or not torch.cuda.is_available()) else "cuda"
        )
        print(f"Running script on {self.device}")
        if self.device != "cpu":
            # empty cache memory, I've encountered GPU mem issues due to fragmentation
            torch.cuda.empty_cache()

        if args.keep_lm:
            # load the model as is, the LM won't be affected
            self.model = Wav2Vec2ForCTC.from_pretrained(
                pretrained_model,
                attention_dropout=0.1,
                hidden_dropout=0.1,
                feat_proj_dropout=0.0,
                mask_time_prob=mask_time_prob,
                layerdrop=0.1,
                ctc_loss_reduction="mean",
                ctc_zero_infinity=True,
                pad_token_id=self.processor.tokenizer.pad_token_id,
            ).to(self.device)
        else:
            # normpath returns only the model name (which is the local path) out of the full name, e.g.,
            # "wav2vec2-large-slavic-parlaspeech-hr" instead of
            # "classla/wav2vec2-large-slavic-parlaspeech-hr"
            if not pretrained_model.startswith("/"):
                local_pretrained = os.path.basename(os.path.normpath(pretrained_model))
            else:
                local_pretrained = pretrained_model
            # now check whether the model exists; download it if not
            check_model_exists_locally(pretrained_model, local_pretrained)

            if os.path.exists(f"{local_pretrained}/pytorch_model.bin"):
                # The newest models don't have a pytorch_model.bin
                state_dict = torch.load(
                    f"{local_pretrained}/pytorch_model.bin", map_location="cpu"
                )
                # the vocab size doesn't (always) match the original model;
                # remove the old LM head manually to initialise a new one
                # otherwise you'll get a size mismatch for lm_head.bias for the new vocabulary size
                if "lm_head.weight" in state_dict:
                    state_dict.pop("lm_head.weight")
                if "lm_head.bias" in state_dict:
                    state_dict.pop("lm_head.bias")

                # We might need to adapt some hyperparams, esp. dropout values. Current values
                # (dropout rate, layer dropout, learning rate) are based on https://arxiv.org/abs/1904.08779
                # pad_token_id: the model's and CTC's blank token.
                # To save GPU mem, enable PyTorch's gradient checkpointing and set loss reduction to "mean"
                self.model = Wav2Vec2ForCTC.from_pretrained(
                    local_pretrained,
                    attention_dropout=0.1,
                    hidden_dropout=0.1,
                    feat_proj_dropout=0.0,
                    mask_time_prob=mask_time_prob,
                    layerdrop=0.1,
                    ctc_loss_reduction="mean",
                    ctc_zero_infinity=True,
                    pad_token_id=self.processor.tokenizer.pad_token_id,
                    state_dict=state_dict,  # added due to different vocab size
                    vocab_size=len(self.processor.tokenizer),
                ).to(self.device)
                del state_dict  # no longer needed
            else:  # no state dict info
                self.model = Wav2Vec2ForCTC.from_pretrained(
                    local_pretrained,
                    attention_dropout=0.1,
                    hidden_dropout=0.1,
                    feat_proj_dropout=0.0,
                    mask_time_prob=mask_time_prob,
                    layerdrop=0.1,
                    ctc_loss_reduction="mean",
                    ctc_zero_infinity=True,
                    pad_token_id=self.processor.tokenizer.pad_token_id,
                    vocab_size=len(self.processor.tokenizer),
                ).to(self.device)

        self.model.freeze_feature_encoder()
        self.model.gradient_checkpointing_enable()

        # set training params
        self.training_args = TrainingArguments(
            output_dir=args.output_model,
            group_by_length=True,
            per_device_train_batch_size=batch_size,
            gradient_accumulation_steps=2,
            save_total_limit=2,  # Only last 2 models are saved. Older ones are deleted.
            no_cuda=no_cuda,  # Use CPU if true; added in the extremely rare case that one can't use CUDA
            #
            fp16=False,  # set to False to save mem. Can only use with CUDA
            evaluation_strategy="epoch",  # alt: IntervalStrategy.STEPS
            load_best_model_at_end=True,  # loads best model after training
            # for the above to work, `save_strategy` needs to be the same as `eval_strategy`;
            # if it's steps (instead of epoch), `save_steps` must be a round multiple of eval_steps
            save_strategy="epoch",  # alt: IntervalStrategy.STEPS
            # eval_accumulation_steps=4,
            # num epochs: found via load_best_model_at_end, I initially let it run for 100 epochs
            num_train_epochs=epochs,
            # save_steps=eval_steps,
            # eval_steps=eval_steps,
            # max_steps=max_steps,
            warmup_steps=int(0.1 * max_steps),  # was: 500
            logging_steps=50,  # was: 200, 10
            learning_rate=learning_rate,  # suggested: 1e-3, 3e-4,
            optim="adamw_torch",  # added because the default ("adamw_hf") throws a warning
            log_level="debug",
            seed=42,  # we can add the seed to the arguments
            metric_for_best_model="eval_wer",  # could also be "eval_loss". Neither produces optimal results
            # set to True if metric_for_best_model is set to a value that you're trying to maximize
            greater_is_better=False,
        )

    def prepare_dataset(self, batch, sampling_rate=16000):
        """prepares the input values and labels for training"""
        # At first I loaded the wav file in a numpy array in the dataset.
        # However, we still need to load it because it's stored as a string in the csv file
        # batch['array'] = np.frombuffer(f"{batch['array']}".encode(), dtype=np.float16)
        # So the alternative is to load the file here.
        batch["array"], _ = librosa.load(batch["path"], sr=sampling_rate)

        batch["input_values"] = self.processor(
            batch["array"], sampling_rate=sampling_rate
        ).input_values[0]

        with self.processor.as_target_processor():
            batch["labels"] = self.processor(batch["sentence"]).input_ids
        return batch

    def compute_metrics(self, pred):
        """compute WER"""
        pred_logits = pred.predictions
        pred_ids = np.argmax(pred_logits, axis=-1)

        pred.label_ids[pred.label_ids == -100] = self.processor.tokenizer.pad_token_id

        pred_str = self.processor.batch_decode(pred_ids)
        # we do not want to group tokens when computing the metrics
        label_str = self.processor.batch_decode(pred.label_ids, group_tokens=False)

        wer_result = wer(truth=label_str, hypothesis=pred_str)
        cer_result = cer(truth=label_str, hypothesis=pred_str)
        return {"wer": wer_result, "cer": cer_result}

    def train_model(self, training_set, eval_set, use_padding=True):
        """start model training/fine-tuning"""
        trainer = Trainer(
            model=self.model,
            data_collator=DataCollatorCTCWithPadding(
                processor=self.processor, padding=use_padding
            ),
            args=self.training_args,
            compute_metrics=self.compute_metrics,
            train_dataset=training_set,
            eval_dataset=eval_set,
            tokenizer=self.processor.feature_extractor,
            callbacks=[EarlyStoppingCallback(early_stopping_patience=5)],
        )
        trainer.train()


class CustomStoreAction(argparse.Action):
    def __init__(self, option_strings, dest, default=None, required=False, help=None):
        super(CustomStoreAction, self).__init__(
            option_strings,
            dest,
            nargs="?",
            const=None,
            default=default,
            required=required,
            help=help,
        )

    def __call__(self, parser, namespace, values, option_string=None):
        if values == "False":
            setattr(namespace, self.dest, False)
        else:
            setattr(namespace, self.dest, True)


def parse_arguments():
    """helper script to parse command line arguments"""
    parser = argparse.ArgumentParser(
        "Script to fine-tune a wav2vec model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--pretrained-model",
        help="The hugging face wav2vec2 model that will be fine-tuned. If you don't know which "
        "one to choose, look for the language family or a related language here: "
        "https://huggingface.co/models?pipeline_tag=automatic-speech-recognition&sort=downloads ",
    )
    parser.add_argument(
        "--lang-family",
        type=str,
        help="In case you don't know which pre-trained model to use and you cannot look it up on "
        "huggingface, provide the language family and a relevant model will be selected for you."
        f"Currently supported options: {list(DEFAULT_MODEL_PER_LANG_FAMILY.keys())}",
    )
    parser.add_argument(
        "--output-model",
        required=True,
        help="The name of the resulting fine-tuned model",
    )
    parser.add_argument(
        "--word-delimiter-token",
        default="|",
        help="The token separating words in the model. The default is a pipe instead of "
        "a space to make it more visible",
    )
    parser.add_argument(
        "--training-dataset-name",
        help="The file name of the .csv training dataset containing the "
        "normalized reference text and the audio file path. See: create_dataset.py",
        required=True,
    )
    parser.add_argument(
        "--eval-dataset-name",
        "--validation-dataset-name",
        help="The file name of the .csv validation dataset containing the "
        "normalized reference text and the audio file path. See: create_dataset.py",
        required=True,
    )
    parser.add_argument(
        "--no-cuda",
        help="Whether to use the CPU only",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "--epochs", help="Number of training epochs", default=35, type=int,
    )
    parser.add_argument(
        "--learning-rate",
        "--lrate",
        help="Training learning rate",
        default=3e-4,
        type=float,
    )
    parser.add_argument(
        "--keep-lm",
        help="Use this option to keep the pretrained models' Language Model. This only "
        "works if the vocabulary of the target language matches the one of the pretrained model",
        default=False,
        action=CustomStoreAction,
    )
    parser.add_argument(
        "--batch-size", type=int, default=8, help="Batch size for processing files"
    )
    parser.add_argument(
        "--mask-time-prob", type=float, default=0.05, help="mask_time_prob. Can also use 0.4"
    )
    parser.add_argument("--max-steps", type=int, default=40000, help="max_steps")
    parser.add_argument(
        "--eval-steps", type=int, default=1000, help="Evaluation step interval"
    )

    return parser.parse_args()


def create_vocab_dict(references, word_delimiter, vocab_fname):
    """prepares the vocab.json file and the labels based on the reference files"""
    alphabet_characters = list(itertools.chain(*list(references)))
    # ignore the word delimiter for now, we'll add it at the very beginning
    sorted_labels = [
        x for x in sorted(set(alphabet_characters)) if x not in [" ", word_delimiter]
    ]
    # add the word delimiter at the beginning
    sorted_labels.insert(0, word_delimiter)
    vocab_dict = {c: i for i, c in enumerate(sorted_labels)}
    # add UNK(ownn) and PAD(ding) characters
    vocab_dict["[UNK]"] = len(vocab_dict)
    vocab_dict["[PAD]"] = len(vocab_dict)

    # save dict to file
    with open(vocab_fname, "w", encoding="utf-8") as vocab_file:
        json.dump(vocab_dict, vocab_file)


@dataclass
class DataCollatorCTCWithPadding:
    """
    Data collator that will dynamically pad the inputs received.
    Args:
        processor (:class:`~transformers.Wav2Vec2Processor`)
            The processor used for proccessing the data.
        padding (:obj:`bool`, :obj:`str` or :
            class:`~transformers.tokenization_utils_base.PaddingStrategy`, `optional`,
        defaults to :obj:`True`):
            Select a strategy to pad the returned sequences (according to the model's padding
            side and padding index)
            among:
            * :obj:`True` or :obj:`'longest'`: Pad to the longest sequence in the batch
                    (or no padding if only a single
              sequence if provided).
            * :obj:`'max_length'`: Pad to a maximum length specified with the argument
                    :obj:`max_length` or to the maximum acceptable input length for the
                    model if that argument is not provided.
            * :obj:`False` or :obj:`'do_not_pad'` (default): No padding (i.e., can output
                a batch with sequences of different lengths).
        max_length (:obj:`int`, `optional`):
            Maximum length of the ``input_values`` of the returned list and
            optionally padding length (see above).
        max_length_labels (:obj:`int`, `optional`):
            Maximum length of the ``labels`` returned list and
            optionally padding length (see above).
        pad_to_multiple_of (:obj:`int`, `optional`):
            If set will pad the sequence to a multiple of the provided value.
            This is especially useful to enable the use of Tensor Cores on NVIDIA
            hardware with compute capability >= 7.5 (Volta).
    """

    processor: Wav2Vec2Processor
    padding: Union[bool, str] = True
    max_length: Optional[int] = None
    max_length_labels: Optional[int] = None
    pad_to_multiple_of: Optional[int] = None
    pad_to_multiple_of_labels: Optional[int] = None

    def __call__(
        self, features: List[Dict[str, Union[List[int], torch.Tensor]]]
    ) -> Dict[str, torch.Tensor]:
        # split inputs and labels since they have to be of different lenghts and need
        # different padding methods
        input_features = [
            {"input_values": feature["input_values"]} for feature in features
        ]
        label_features = [{"input_ids": feature["labels"]} for feature in features]

        batch = self.processor.pad(
            input_features,
            padding=self.padding,
            max_length=self.max_length,
            pad_to_multiple_of=self.pad_to_multiple_of,
            return_tensors="pt",
        )
        with self.processor.as_target_processor():
            labels_batch = self.processor.pad(
                label_features,
                padding=self.padding,
                max_length=self.max_length_labels,
                pad_to_multiple_of=self.pad_to_multiple_of_labels,
                return_tensors="pt",
            )

        # replace padding with -100 to ignore loss correctly
        labels = labels_batch["input_ids"].masked_fill(
            labels_batch.attention_mask.ne(1), -100
        )

        batch["labels"] = labels
        return batch


def get_model_name(model_name, language_family):
    """The user can either provide a model name or the languge family.
    Make sure that either information is available and that the final
    model exists locally; if not, download it."""
    if not (model_name or language_family):
        sys.exit(
            "Please provide a pretrained model (--model-name) or "
            "the language family (--lang-family)"
        )

    if language_family and not model_name:
        # get the lang family model, or the (most) multilingual one as default
        model_name = DEFAULT_MODEL_PER_LANG_FAMILY.get(
            language_family, "voidful/wav2vec2-xlsr-multilingual-56"
        )
    print(f"Using model: {model_name}")
    # return the model name
    return model_name


def check_model_exists_locally(pretrained_model, local_pretrained=None):
    """ensure that a hugging face wav2vec2 pre-trained model exists locally.
    Download it if not"""
    if not local_pretrained:
        local_pretrained = os.path.basename(os.path.normpath(pretrained_model))
    # All models have a vocab.json file; check if the file exists locally
    if not os.path.isfile(f"{local_pretrained}/config.json"):
        print(
            "The pre-trained model was not found locally. Downloading automatically "
            f"from: https://huggingface.co/{pretrained_model}"
        )

        Repo.clone_from(f"https://huggingface.co/{pretrained_model}", local_pretrained)

        print("Downloaded successfully.")


if __name__ == "__main__":
    args = parse_arguments()

    # load training and eval datasets
    data_files = {"train": args.training_dataset_name, "eval": args.eval_dataset_name}
    dataset = load_dataset("csv", data_files=data_files)

    training_dataset = dataset["train"]
    eval_dataset = dataset["eval"]

    # create the vocab.json file
    create_vocab_dict(
        references=training_dataset["sentence"],
        word_delimiter=args.word_delimiter_token,
        vocab_fname="tmp_vocab.json",
    )

    pretrained_model = get_model_name(
        args.pretrained_model, language_family=args.lang_family
    )
    ft_trainer = Wav2VecFineTuner(
        vocab_json_file="tmp_vocab.json",
        word_delimiter_token=args.word_delimiter_token,
        pretrained_model=pretrained_model,
        output_model_name=args.output_model,
        no_cuda=args.no_cuda,
        epochs=args.epochs,
        learning_rate=args.learning_rate,
        batch_size=args.batch_size,
        mask_time_prob=args.mask_time_prob,
        max_steps=args.max_steps,
        eval_steps=args.eval_steps,
    )

    # the following pre-processing is using multi-processing but is non-batched.
    # To use batched, change prepare_dataset() to load
    # the wav files in batches and add batched=True, batch_size=16 arguments to map()
    training_dataset = training_dataset.map(ft_trainer.prepare_dataset)
    eval_dataset = eval_dataset.map(ft_trainer.prepare_dataset)

    # remove redundant columns
    training_dataset = training_dataset.remove_columns(["array", "path", "sentence"])
    eval_dataset = eval_dataset.remove_columns(["array", "path", "sentence"])

    # start training
    ft_trainer.train_model(training_set=training_dataset, eval_set=eval_dataset)

    # remove temporary vocab.json at the end of training
    if os.path.exists("tmp_vocab.json"):
        os.remove("tmp_vocab.json")
