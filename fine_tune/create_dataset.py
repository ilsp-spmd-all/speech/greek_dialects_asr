"""Helper script to create a dataset with a pre-processed reference text and the wav file path"""

import argparse
import os
import sys
from pathlib import Path

import librosa
import pandas as pd
from asr_text_normalizer.normalizer import cfg, normalize_txt


def create_dataset_csv(
    corpus_path,
    dataset_name,
    duration_threshold,
    split_dataset,
    training_decimal_fraction,
    eval_decimal_fraction,
    test_decimal_fraction,
    seed,
    preprocess,
    do_lowercase,
    encoding,
    lang_code,
    excluded_strings,
    text_suffix
):
    """Create a csv containing the normalized reference and the speech arrays.
    @corpus_path: the path containing wav/mp3 files and their reference txt files
    @dataset_name: the output csv dataset name
    @duration_threshold: if the threshold is >0, all audio files that have longer duration
                        than the threshold (in seconds) will be excluded
    @split_dataset: if True, the dataset will be split into training, eval, and test set.
                    The exact decimal fractions are determined via the following three arguments.
    @training_decimal_fraction: For instance, if split_training_fraction is 0.8, the training set
                                will be 8)% of the full dataset. The "training_" prefix will be added.
    @eval_decimal_fraction: Fraction of the evaluation set. The "eval_" prefix will be added.
    @test_decimal_fraction: Fraction of the test set. The "test_" prefix will be added.
    @seed: Use a seed to be able to replicate the dataset generation
    @preprocess: If False, skip text pre-processing and load the reference text as is
    @do_lowercase: Whether to lowercase the reference text
    @encoding: For most texts it's utf-8.
    @lang_code: "pk" for Pomak, "en" for English etc. It is only needed for number-into-words conversion
                during text preprocessing.
    @excluded_strings: List containing audio file substrings that will be excluded from the dataset.
                       This is useful, for instance, if you want to exclude a speaker or a specific file .
    """
    if preprocess:
        normalizer_config = cfg.NormalizeConfig(
            lang=lang_code,
            replace_characters=False,
            do_lowercase=do_lowercase,
            remove_accents=False,
            lexicalize_dates_and_numbers=True,
            remove_punctuation=True,
        )
    audio_paths = Path(corpus_path).rglob("*.[wWm][aAp][vV3]")  # wav, WAV, or mp3
    normalized_ref = []
    paths = []
    for audio_fname in audio_paths:
        if excluded_strings:
            # ignore audio files containing one or more excluded strings
            if any(pattern in str(audio_fname) for pattern in excluded_strings):
                print("Skipping:", audio_fname)
                continue
        if duration_threshold:
            duration = librosa.get_duration(filename=audio_fname)
            if duration > duration_threshold:
                # skipping audio file because it's too long
                print("Skipping:", audio_fname, f"({duration}s)")
                continue
        reference_fname = f"{os.path.splitext(audio_fname)[0]}.{text_suffix}"
        # proceed only if there's a reference file as well
        if os.path.isfile(reference_fname):
            if not preprocess:
                # in some cases, the transcription is already pre-processed; just load it as is.
                with open(reference_fname, "r", encoding=encoding) as file:
                    normalized_text = file.read().replace("\n", " ")
            else:
                normalized_text = normalize_txt(
                    txt_path=reference_fname,
                    encoding=encoding,
                    config=normalizer_config,
                    nj=1,
                    out_file=None,
                )
                # the output is a list, convert to a string
                normalized_text = " ".join(normalized_text)

            if normalized_text:
                # make sure that the normalized text contains words
                normalized_ref.append(normalized_text)
                paths.append(audio_fname)
    dataset = pd.DataFrame({"path": paths, "sentence": normalized_ref})
    # write to csv without an index column

    if split_dataset:
        if (
            training_decimal_fraction + eval_decimal_fraction + test_decimal_fraction
            != 1
        ):
            sys.exit(
                "Make sure that the total decimal fraction of the training, evaluation, "
                " and test set splits are 1.0. Current sum: "
                f"{training_decimal_fraction + eval_decimal_fraction + test_decimal_fraction}"
            )
        training_set = dataset.sample(frac=training_decimal_fraction, random_state=seed)
        # Creating dataframe with rest of the values
        testeval_set = dataset.drop(training_set.index)
        # Sample evaluation set. Note that you need to resample the evaluation dataset's
        # fraction, otherwise you'll sample 10% of the testeval set instead of the total one
        updated_eval_fraction = (1.0 * eval_decimal_fraction) / (
            eval_decimal_fraction + test_decimal_fraction
        )
        eval_set = testeval_set.sample(frac=updated_eval_fraction, random_state=seed)
        test_set = testeval_set.drop(eval_set.index)
        # Save the three datasets
        training_set.to_csv(f"training_{dataset_name}", index=False)
        if not eval_set.empty:
            eval_set.to_csv(f"eval_{dataset_name}", index=False)
        if not test_set.empty:
            test_set.to_csv(f"test_{dataset_name}", index=False)
    else:  # no splitting into training/eval/test
        dataset.to_csv(dataset_name, index=False)


class CustomStoreTrue(argparse.Action):
    def __init__(self, option_strings, dest, default=None, required=False, help=None):
        super(CustomStoreTrue, self).__init__(
            option_strings,
            dest,
            nargs="?",
            const=None,
            default=default,
            required=required,
            help=help,
        )

    def __call__(self, parser, namespace, values, option_string=None):
        if values in ["False", "false"]:
            setattr(namespace, self.dest, False)
        else:
            setattr(namespace, self.dest, True)


class CustomStoreFalse(argparse.Action):
    def __init__(self, option_strings, dest, default=None, required=False, help=None):
        super(CustomStoreFalse, self).__init__(
            option_strings,
            dest,
            nargs="?",
            const=None,
            default=default,
            required=required,
            help=help,
        )

    def __call__(self, parser, namespace, values, option_string=None):
        if values in ["True", "true"]:
            setattr(namespace, self.dest, True)
        else:
            setattr(namespace, self.dest, False)


def parse_arguments():
    """helper script to parse command line arguments"""
    parser = argparse.ArgumentParser(
        "Script to create a dataset in a csv format in order to "
        "use it with the dataloader when fine-tuning a model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--corpus-dir",
        type=str,
        help="A path to the corpus containing audio-transcript pairs (i.e., audio files (*.wav)"
        "and the respective reference files (e.g., *.txt).",
        required=True,
    )
    parser.add_argument(
        "--text-suffix",
        default="txt",
        help="Text file suffix to look for.",
    )
    parser.add_argument(
        "--dataset-name",
        help="The csv name of the output dataset. Unless you have used --no-split-dataset, "
        "the dataset will be split and the prefixes 'test_', 'eval_' and 'training_' "
        "will be added to the name.",
        required=True,
    )
    parser.add_argument(
        "--cutoff-duration",
        type=float,
        default=30,
        help="Only include audio segments that have a total duration less than the threshold "
        "(in seconds). If you do not want that, give a really large threshold.",
    )
    parser.add_argument(
        "--no-split-dataset",
        default=True,
        dest="split_dataset",
        action=CustomStoreFalse,
        help="By default the dataset is split into training, eval, and test datasets. "
        "If you do NOT want to split the dataset, use the --no-split-dataset flag. "
        "Otherwise, the dataset will be split and the corresponding prefix will be added "
        "to each split (e.g., test_dataset.csv). The default split fractions are 0.8-0.1-0.1 "
        "for training, eval and test. Use the corresponding flags, e.g., --test-decimal-fraction, to change these.",
    )
    parser.add_argument(
        "--training-decimal-fraction",
        type=float,
        default=0.8,
        help="Decimal fraction of the training set. It is ignored if you use --no-split-dataset.",
    )
    parser.add_argument(
        "--eval-decimal-fraction",
        type=float,
        default=0.1,
        help="Decimal fraction of the evaluation set. It is ignored if you use --no-split-dataset.",
    )
    parser.add_argument(
        "--test-decimal-fraction",
        type=float,
        default=0.1,
        help="Decimal fraction of the test set. It is ignored if you use --no-split-dataset.",
    )
    parser.add_argument(
        "--seed",
        default=42,
        type=int,
        help="This is used for reproducability together with the training "
        "fraction when splitting into training and test sets",
    )
    parser.add_argument(
        "--encoding",
        help="Text file encoding.",
        default="utf-8",
    )
    parser.add_argument(
        "--lang-code",
        help="Language code; it is mainly needed for number/date conversion.",
        default="pk",
    )
    parser.add_argument(
        "--preprocess",
        dest="preprocess",
        action=CustomStoreTrue,
        default=False,
        help="Use this flag if the transcript is *not* normalized and needs to be processed.",
    )
    parser.add_argument(
        "--no-lowercasing",
        dest="lowercase",
        action=CustomStoreFalse,
        default=True,
        help="Use this flag to disable text lowercasing when pre-processing. The destination value "
        "is 'lowercase' which, by default, is set to True.",
    )
    parser.add_argument(
        "--exclude-files",
        nargs="*",
        default=None,
        help="To exclude certain speakers or file ids from the dataset, give the corresponding speaker/file ids. "
        "For instance, --exclude-speakers 9G75fk will exclude all files from speaker 9G75fk. "
        "--exclude-speakers 9G75fk R_01 will additionally exclude the audio file R_01 by any speaker",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()

    # create a csv file with the normalized reference sentence(s) and the wav file path
    create_dataset_csv(
        corpus_path=args.corpus_dir,
        dataset_name=args.dataset_name,
        duration_threshold=args.cutoff_duration,
        split_dataset=args.split_dataset,
        training_decimal_fraction=args.training_decimal_fraction,
        eval_decimal_fraction=args.eval_decimal_fraction,
        test_decimal_fraction=args.test_decimal_fraction,
        seed=args.seed,
        preprocess=args.preprocess,
        do_lowercase=args.lowercase,
        encoding=args.encoding,
        lang_code=args.lang_code,
        excluded_strings=args.exclude_files,
        text_suffix=args.text_suffix
    )
