"""
Fine-tune a Whisper model
https://colab.research.google.com/github/sanchit-gandhi/notebooks/blob/main/fine_tune_whisper.ipynb#scrollTo=-2zQwMfEOBJq
"""

import argparse
import os
from dataclasses import dataclass
from typing import Dict, List, Union

if "CUDA_VISIBLE_DEVICES" not in os.environ:
    # it's important that this is set BEFORE pytorch/transformers are imported
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"

from dataclasses import dataclass
from typing import Any, Dict, List, Union

import librosa
import torch
from datasets import load_dataset
from jiwer import cer, wer
from transformers import (
    Seq2SeqTrainer,
    Seq2SeqTrainingArguments,
    WhisperFeatureExtractor,
    WhisperForConditionalGeneration,
    WhisperProcessor,
    WhisperTokenizer,
)


class WhisperFineTuner:
    """class with helper scripts to allow fine-tuning of a wav2vec2 model"""

    def __init__(
        self,
        whisper_model,
        output_model_name,
        epochs,
        learning_rate,
        batch_size,
        max_steps,
        eval_steps,
        language="Greek",
    ):
        # do not set the specific cuda device here; use CUDA_VISIBLE_DEVICES instead
        self.device = torch.device("cpu" if (not torch.cuda.is_available()) else "cuda")
        print(f"Running script on {self.device}")

        if self.device != "cpu":
            # empty cache memory, I've encountered GPU mem issues due to fragmentation
            torch.cuda.empty_cache()

        self.feature_extractor = WhisperFeatureExtractor.from_pretrained(whisper_model)
        self.tokenizer = WhisperTokenizer.from_pretrained(
            whisper_model, language=language, task="transcribe"
        )

        self.model = WhisperForConditionalGeneration.from_pretrained(whisper_model)
        self.model.generation_config.language = language
        self.model.generation_config.task = "transcribe"
        # this was the legacy way of setting the language and task arguments
        self.model.generation_config.forced_decoder_ids = None

        self.processor = WhisperProcessor.from_pretrained(
            whisper_model, language=language, task="transcribe"
        )
        # initialize data collator
        self.data_collator = DataCollatorSpeechSeq2SeqWithPadding(
            processor=self.processor,
            decoder_start_token_id=self.model.config.decoder_start_token_id,
        )
        self.processor.save_pretrained(output_model_name)

        self.training_args = Seq2SeqTrainingArguments(
            output_dir=args.output_model,
            per_device_train_batch_size=batch_size,
            gradient_accumulation_steps=2,  # increase by 2x for every 2x decrease in batch size
            learning_rate=learning_rate,
            warmup_steps=500,  # int(0.1 * max_steps)
            max_steps=5000,
            gradient_checkpointing=True,
            fp16=True,  # set to False to save mem. Can only use with CUDA
            evaluation_strategy="steps",  # epoch?
            per_device_eval_batch_size=8,
            predict_with_generate=True,
            generation_max_length=225,
            save_steps=eval_steps,
            eval_steps=eval_steps,
            logging_steps=50,
            # report_to=["tensorboard"],
            load_best_model_at_end=True,
            metric_for_best_model="eval_wer",  # wer?
            greater_is_better=False,
            push_to_hub=False,
            seed=42,
            log_level="debug",
        )


    def prepare_dataset(self, batch, sampling_rate=16000):
        """prepares the input values and labels for training"""
        batch["array"], _ = librosa.load(batch["path"], sr=sampling_rate)
        # compute log-Mel input features from input audio array
        batch["input_features"] = self.feature_extractor(
            batch["array"], sampling_rate=sampling_rate
        ).input_features[0]

        # encode target text to label ids
        batch["labels"] = self.tokenizer(batch["sentence"]).input_ids
        return batch

    def compute_metrics(self, pred):
        """compute WER"""
        pred_ids = pred.predictions
        label_ids = pred.label_ids

        # replace -100 with the pad_token_id
        label_ids[label_ids == -100] = self.tokenizer.pad_token_id

        # we do not want to group tokens when computing the metrics
        pred_str = self.tokenizer.batch_decode(pred_ids, skip_special_tokens=True)
        label_str = self.tokenizer.batch_decode(label_ids, skip_special_tokens=True)

        wer_result = wer(truth=label_str, hypothesis=pred_str)
        cer_result = cer(truth=label_str, hypothesis=pred_str)
        return {"wer": wer_result, "cer": cer_result}

    def train_model(self, training_set, eval_set):
        """start model training/fine-tuning"""
        trainer = Seq2SeqTrainer(
            args=self.training_args,
            model=self.model,
            train_dataset=training_set,
            eval_dataset=eval_set,
            data_collator=self.data_collator,
            compute_metrics=self.compute_metrics,
            tokenizer=self.processor.feature_extractor,
        )
        trainer.train()


def parse_arguments():
    """helper script to parse command line arguments"""
    parser = argparse.ArgumentParser(
        "Script to fine-tune a wav2vec model",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--whisper-model",
        help="The Whisper model that will be fine-tuned.",
        default="openai/whisper-medium",
    )
    parser.add_argument(
        "--output-model",
        required=True,
        help="The name of the resulting fine-tuned model",
    )
    parser.add_argument(
        "--word-delimiter-token",
        default="|",
        help="The token separating words in the model. The default is a pipe instead of "
        "a space to make it more visible",
    )
    parser.add_argument(
        "--training-dataset-name",
        help="The file name of the .csv training dataset containing the "
        "normalized reference text and the audio file path. See: create_dataset.py",
        required=True,
    )
    parser.add_argument(
        "--eval-dataset-name",
        "--validation-dataset-name",
        help="The file name of the .csv validation dataset containing the "
        "normalized reference text and the audio file path. See: create_dataset.py",
        required=True,
    )
    parser.add_argument(
        "--no-cuda",
        help="Whether to use the CPU only",
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "--epochs",
        help="Number of training epochs",
        default=35,
        type=int,
    )
    parser.add_argument(
        "--learning-rate",
        "--lrate",
        help="Training learning rate",
        default=3e-4,
        type=float,
    )
    parser.add_argument(
        "--batch-size", type=int, default=8, help="Batch size for processing files"
    )
    parser.add_argument("--max-steps", type=int, default=40000, help="max_steps")
    parser.add_argument(
        "--steps-eval", type=int, default=1000, help="Evaluation step interval"
    )

    return parser.parse_args()


@dataclass
class DataCollatorSpeechSeq2SeqWithPadding:
    processor: Any
    decoder_start_token_id: int

    def __call__(
        self, features: List[Dict[str, Union[List[int], torch.Tensor]]]
    ) -> Dict[str, torch.Tensor]:
        # split inputs and labels since they have to be of different lengths and need different padding methods
        # first treat the audio inputs by simply returning torch tensors
        input_features = [
            {"input_features": feature["input_features"]} for feature in features
        ]
        batch = self.processor.feature_extractor.pad(
            input_features, return_tensors="pt"
        )

        # get the tokenized label sequences
        label_features = [{"input_ids": feature["labels"]} for feature in features]
        # pad the labels to max length
        labels_batch = self.processor.tokenizer.pad(label_features, return_tensors="pt")

        # replace padding with -100 to ignore loss correctly
        labels = labels_batch["input_ids"].masked_fill(
            labels_batch.attention_mask.ne(1), -100
        )

        # if bos token is appended in previous tokenization step,
        # cut bos token here as it's append later anyways
        if (labels[:, 0] == self.decoder_start_token_id).all().cpu().item():
            labels = labels[:, 1:]

        batch["labels"] = labels

        return batch


if __name__ == "__main__":
    args = parse_arguments()

    # load training and eval datasets
    data_files = {"train": args.training_dataset_name, "eval": args.eval_dataset_name}
    dataset = load_dataset("csv", data_files=data_files)

    training_dataset = dataset["train"]
    eval_dataset = dataset["eval"]

    ft_trainer = WhisperFineTuner(
        whisper_model=args.whisper_model,
        output_model_name=args.output_model,
        epochs=args.epochs,
        learning_rate=args.learning_rate,
        batch_size=args.batch_size,
        max_steps=args.max_steps,
        eval_steps=args.steps_eval,
    )

    # the following pre-processing is using multi-processing but is non-batched.
    # To use batched, change prepare_dataset() to load
    # the wav files in batches and add batched=True, batch_size=16 arguments to map()
    training_dataset = training_dataset.map(ft_trainer.prepare_dataset)
    eval_dataset = eval_dataset.map(ft_trainer.prepare_dataset)

    # remove redundant columns
    training_dataset = training_dataset.remove_columns(["array", "path", "sentence"])
    eval_dataset = eval_dataset.remove_columns(["array", "path", "sentence"])

    # start training
    ft_trainer.train_model(training_set=training_dataset, eval_set=eval_dataset)
